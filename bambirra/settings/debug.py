from .base import *

INTERNAL_IPS = ['127.0.0.1']

DEBUG = True

DEBUG_APPS = ['debug_toolbar']

INSTALLED_APPS += DEBUG_APPS

MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']

