from django.apps import AppConfig


class FluxodecaixaConfig(AppConfig):
    name = 'fluxo_de_caixa'
    # verbose_name = 'Fluxo de Caixa'
    verbose_name = 'Administrativo'
