from datetime import date
from dateutil.relativedelta import relativedelta
from dal import autocomplete
from django.db.models import Q

from .models import models


class ContaBancariaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.ContaBancaria.objects.none()

        qs = models.ContaBancaria.objects.all()

        if self.q:
            words = self.q.split(' ')
            for word in words:
                word_q = word
                qs = qs.filter(
                    Q(nome__icontains=word_q) | \
                    Q(slug__icontains=word_q)
                )

        return qs


class OrcamentoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.Orcamento.objects.none()

        qs = models.Orcamento.objects.all()

        if self.q:
            words = self.q.split(' ')
            for word in words:
                word_q = word
                qs = qs.filter(
                    Q(projeto__nome__icontains=word_q) | \
                    Q(projeto__slug__icontains=word_q) | \
                    Q(nome__icontains=word_q) | \
                    Q(slug__icontains=word_q)
                )

        return qs


class ProjetoAtividadeAutocomplete(autocomplete.Select2QuerySetView):

    def get_atividade_orcamento(self):
        return models.OrcamentoAtividade.objects.all()\
                .order_by(
                        '-mes_ultima_retirada')

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.OrcamentoAtividade.objects.none()

        # TODO: Carregar listagem com filtro padrão
        #       para melhorar performance.
        #       ex.: Projetos ativos
        qs = self.get_atividade_orcamento()

        if self.q:
            words = self.q.split(' ')
            for word in words:
                word_q = word
                qs = qs.filter(
                    Q(orcamento__projeto__nome__icontains=word_q) | \
                    Q(orcamento__projeto__slug__icontains=word_q) | \
                    Q(orcamento__nome__icontains=word_q) | \
                    Q(orcamento__slug__icontains=word_q) | \
                    Q(atividade__nome__icontains=word_q) | \
                    Q(atividade__slug__icontains=word_q)
                )

        return qs


class ProjetoAtividadeAtivoAutocomplete(ProjetoAtividadeAutocomplete):

    def get_atividade_orcamento(self):
        # orcamento__encerramento >= atual
        data_limite = date.today() - relativedelta(months=1)
        return models.OrcamentoAtividade.objects\
            .filter(
                Q(orcamento__encerramento__isnull=True) & \
                (Q(encerramento__gte=data_limite) | \
                 Q(encerramento=None))
            )\
            .exclude(
                Q(orcamento__projeto__slug__icontains='ferias') | \
                Q(orcamento__projeto__slug__icontains='descanso') | \
                Q(orcamento__projeto__slug__icontains='atestado-medico')
            )\
            .order_by('-mes_ultima_retirada')


class CategoriaEntradaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.CategoriaEntrada.objects.none()

        qs = models.CategoriaEntrada.objects.all()

        if self.q:
            # TODO criar campo slug para melhorar a busca
            qs = qs.filter(nome__icontains=self.q)

        return qs


class CategoriaPagamentoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.CategoriaPagamento.objects.none()

        qs = models.CategoriaPagamento.objects.all()

        if self.q:
            # TODO criar campo slug para melhorar a busca
            qs = qs.filter(nome__icontains=self.q)

        return qs


class ProjetoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.Projeto.objects.none()

        qs = models.Projeto.objects.all()

        if self.q:
            qs = qs.filter(
                Q(nome__icontains=self.q) | \
                Q(slug__icontains=self.q)
            )

        return qs
