Fluxo de caixa: Aplicação principal
###################################

Esta aplicação tem por objetivo:
* Cadastro de cooperadas para uso da plataforma;
* Registro de projetos e orçamentos;
* Gerenciamento das horas e prazo das atividades do orçamento aprovado;
* Registro de horas trabalhadas por atividade de orçamento;
* Registro de entradas e saídas financeiras em diferentes contas bancárias.


Cadastro de cooperadas para uso da plataforma
*********************************************

...

Modelo de dados
===============

...

.. kroki:: ./modelo_fluxo_caixa-cadastro_acesso_cooperadas.dot png
