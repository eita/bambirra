from datetime import date, datetime
from decimal import Decimal
from django.http import JsonResponse
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import models
from . import utils


#########################################
# FINANCEIRO: SALDO DE DETERMINADA CONTA
#########################################

def conta_saldo(request, pk):

    # checar se requisição é autenticada

    if not request.user.is_authenticated:
        data_error = {'status': 403,
                      'content': 'Favor autenticar-se'}
        return JsonResponse(data_error)

    # Obter lançamento imediatamente anterior ao da requisição

    # obtem instancia do lançamento
    lancamento_id = request.GET.get('lancamento', None)
    if not lancamento_id:
        data_error = {}
        return JsonResponse(data_error)

    if request.GET['data']:
        data_lanc = request.GET['data'].split('/')
        data_movimento = f"{data_lanc[2]}-{data_lanc[1]}-{data_lanc[0]}"
    else:
        data_movimento = date.today()

    conta = models.ContaBancaria.objects.get(pk=pk)

    # Caso seja uma inserção, considerar ...
    if not lancamento_id:

        ## Primeiro lançamento, filtrado por data movimento <= a data selecionada
        ## ordenado pelo numero lançamento decrescente.
        lancamento_anterior = models.Movimento.objects\
                .filter(Q(data__lte=data_movimento) | Q(data__isnull=True), conta=pk)\
                .order_by('-data', '-posicao_lancamento')\
                .first()
        if lancamento_anterior:
            saldo = lancamento_anterior.conta_saldo
        else:
            saldo = conta.saldo_valor

    # Caso seja uma atualização, considerar ...
    else:

        ## instancia.posicao_lançamento:
        try:
            lancamento = models.Movimento.objects.get(pk=lancamento_id)
        except:
            data_error = {'status': 404,
                    'content': 'Lançamento não encontrado!'}
            return JsonResponse(data_error)
        posicao_lancamento = lancamento.posicao_lancamento

        ## filtrar o lançamento imediatamente anterior, considerando o banco selecionado.
        lancamento_anterior = models.Movimento.objects\
                .filter(conta_id=pk,
                        data__lte=data_movimento,
                        posicao_lancamento__lt=posicao_lancamento)\
                .order_by('-data', '-posicao_lancamento')\
                .first()
        if lancamento_anterior:
            saldo = lancamento_anterior.conta_saldo
        else:
            saldo = conta.saldo_valor
        #import pdb; pdb.set_trace()

    # Caso a coluna saldo do lançamento encontrado esteja vazia ...
    if not saldo:
        # obter saldo direto da conta
        saldo = conta.saldo_valor

        ## Avisar que o calculo de saldo deve ser atualizado (?)

    movimento = models.Movimento.objects\
            .filter(conta=pk)\
            .exclude(conta_saldo=None)\
            .order_by('-data', '-posicao_lancamento')\
            .first()

    if movimento:
        saldo_atual = movimento.conta_saldo
    else:
        saldo_atual = 'Nenhum valor encontrado'

    if lancamento_anterior:
        saldo_data = lancamento_anterior.data
        if saldo_data:
            saldo_data = saldo_data.strftime('%d/%m/%y')
        else:
            saldo_data = None
    else:
        saldo_data = conta.saldo_atualizacao

    data = {
        'status': 200,
        'conta': f"{conta}",
        'saldo': f'{saldo}',
        'saldo_data': f'{saldo_data}',
        'saldo_atual': f'{saldo_atual}'
    }
    return JsonResponse(data)


##########################
# FINANCEIRO: LIVRO CAIXA
##########################

def gerar_livro_caixa(request):

    # checar se requisição é autenticada

    if not request.user.is_authenticated:
        data_error = {'status': 403,
                      'content': 'Favor autenticar-se'}
        return JsonResponse(data_error)

    # Chamar função para gerar o livro caixa
    data = {
        'month': request.GET['mes'],
        'year': request.GET['ano']
    }
    livro_caixa_url = utils.gerar_livro_caixa(**data)

    data = {
        'status': 200,
        'url_livro_caixa': livro_caixa_url
    }
    return JsonResponse(data)


###################
# DIREITOS: FÉRIAS
###################

def retirada_ferias_saldo(request):

    # checar se requisição é autenticada
    if not request.user.is_authenticated:
        data_error = {'status': 403,
                      'content': 'Favor autenticar-se'}
        return JsonResponse(data_error)

    data = request.GET
    usuaria_id = data.get('usuaria', None)
    dias = data.get('dias', None)
    retirada_ano = data.get('retirada_ano', None)
    if not retirada_ano:
        retirada_ano = date.today().year

    if not usuaria_id:
        data_error = {'status': 204,
                      'content': f'Por favor, selecione a cooperada.'}
        return JsonResponse(data_error)

    if not dias:
        data_error = {'status': 204,
                      'content': f'Por favor, informe o numero de dias de férias.'}
        return JsonResponse(data_error)

    dias = int(dias)

    # get pessoa
    pessoa = models.Cooperada.objects.get(pk=usuaria_id)

    if retirada_ano == date.today().year:
        pessoa.update_ferias_saldo_ano_atual()

    media_horas_ferias = pessoa.calc_media_horas_ferias(
        datetime.strptime(f'{retirada_ano}-01-01', '%Y-%m-%d')
    )
    media_horas_ferias = round(Decimal(media_horas_ferias), 2)
    media_horas_ferias_ref_de, media_horas_ferias_ref_ate = pessoa.get_media_horas_ferias_periodo(
        datetime.strptime(f'{retirada_ano}-01-01', '%Y-%m-%d'), verbose_format=True
    )
    media_horas_ferias = f"Média de horas ref. a \
        {media_horas_ferias_ref_de} até {media_horas_ferias_ref_ate}: \
        <b>{str(media_horas_ferias).replace('.', ',')}</b>"

    qs_ferias_saldo = pessoa.ferias_saldo\
        .filter(ano__lte=retirada_ano)\
        .exclude(saldo__isnull=True)
    if not qs_ferias_saldo:
        data_error = {'status': 204,
                      'content': f'Desculpe, você não tem saldo de férias.'}
        return JsonResponse(data_error)

    # checar se tem retirada_id
    retirada_id = data.get('retirada_id', None)
    if retirada_id:
        # obter retirada
        retirada = models.Retirada.objects.get(pk=retirada_id)
        # obter registros de férias para este retirada_id
        ferias_retirada = retirada.registro_ferias.all()

    ferias_saldo = {}
    atualizar_saldo = True
    dias_ano = dias
    saldo_total_periodo = 0
    for ano_saldo_obj in qs_ferias_saldo:
        try:
            saldo = ferias_retirada.get(
                cooperada_ferias_saldo=ano_saldo_obj
            ).saldo_anterior
        except:
            saldo = ano_saldo_obj.saldo

        if not saldo:
            continue

        saldo_total_periodo = saldo_total_periodo + saldo

        ferias_saldo[ano_saldo_obj.ano] = {
            'saldo_anterior': saldo,
            'valor_hora': ano_saldo_obj.get_valor_hora()
        }
        if atualizar_saldo:
            saldo_atualizado = saldo - dias_ano
        else:
            saldo_atualizado = saldo

        if saldo_atualizado >= 0:
            if saldo_atualizado:
                ferias_saldo[ano_saldo_obj.ano]['saldo_atualizado'] = saldo_atualizado
            else:
                ferias_saldo[ano_saldo_obj.ano]['saldo_atualizado'] = 'cumprida'
            atualizar_saldo = False
        else:
            ferias_saldo[ano_saldo_obj.ano]['saldo_atualizado'] = 'cumprida'
            dias_ano = saldo_atualizado * (-1)

    if (not ferias_saldo
            or (dias > saldo_total_periodo)):
        data_error = {'status': 204,
                      'content': f'Desculpe, você não tem saldo de férias.'}
        return JsonResponse(data_error)

    json = {
        'status': 200,
        'ferias_saldo': ferias_saldo,
        'media_horas_ferias': media_horas_ferias
    }
    return JsonResponse(json)


#####################
# DIREITOS: DESCANSO
#####################

def retirada_horas_descanso(request):

    # checar se requisição é autenticada
    if not request.user.is_authenticated:
        data_error = {'status': 403,
                      'content': 'Favor autenticar-se'}
        return JsonResponse(data_error)

    data = request.GET
    horas_atividades = data.get('horas_atividades', '')
    if not horas_atividades:
        return {
            'status': 200,
            'total_horas_atividades': 0
        }
    horas_atividades = horas_atividades.split('|')

    total_horas_atividades = 0
    for horas in horas_atividades:
        horas_clean = models.converter_horas_textual(horas)
        try:
            total_horas_atividades += horas_clean
        except TypeError as err:
            if 'float' in str(type(total_horas_atividades)):
                total_horas_atividades += float(horas_clean)
            else:
                total_horas_atividades += Decimal(horas_clean)

    horas_descanso = 20;
    horas_min_descanso_integral = 80;
    descanso = 'integral'

    """
    Caso férias esteja preenchido, descontar o descanso:
    Descontar o percentual de dias que ficou em férias no mês.
    """
    dias_ferias = data.get('dias_ferias', 0)
    if dias_ferias:
        dias_ferias = int(dias_ferias)
        # calculo percentual de dias em férias
        # TODO melhorar contabilização de dias no mês, considerando mês com 30 e 31 dias
        """
        dias_mes    - 100
        dias_ferias - x
        """
        dias_mes = 31
        percentual_ferias = (dias_ferias * 100) / dias_mes
        percentual_disponibilidade = 100 - percentual_ferias

        # calculo horas_descanso considerando percentual de férias
        """
        horas_descanso - 100
        x              - percentual_disponibilidade
        """
        horas_descanso = (horas_descanso * percentual_disponibilidade) / 100

        # calculo horas_min_descanso_integral considerando percentual de férias
        """
        horas_min_descanso_integral - 100
        x                           - percentual_disponibilidade
        """
        horas_min_descanso_integral = (horas_min_descanso_integral * percentual_disponibilidade) / 100

    if total_horas_atividades < horas_min_descanso_integral:
        horas_descanso = (horas_descanso * float(total_horas_atividades)) / horas_min_descanso_integral
        descanso = 'proporcional'

    data_return = {
        'status': 200,
        'total_horas_atividades': round(Decimal(total_horas_atividades), 2),
        'horas_descanso': round(Decimal(horas_descanso), 2),
        'descanso': descanso
    }
    return JsonResponse(data_return)

def retirada_update_horas_descanso(request):

    # checar se requisição é autenticada
    if not request.user.is_authenticated:
        data_error = {'status': 403,
                      'content': 'Favor autenticar-se'}
        return JsonResponse(data_error)

    data = request.GET
    retirada_id = data.get('retirada_id', '')
    horas_descanso = data.get('horas_descanso', '')
    orcamento_atividade_descanso = data.get('orcamento_atividade_descanso', '')
    projeto_descanso = data.get('projeto_descanso', '')

    retirada = models.Retirada.objects.get(pk=retirada_id)
    descanso_obj, create = retirada.projetos.get_or_create(
        orcamento_atividade_id=orcamento_atividade_descanso,
        projeto_id=projeto_descanso,
    )
    descanso_obj.horas_trabalhadas_textual=horas_descanso
    descanso_obj.horas=horas_descanso
    descanso_obj.save()

    data_return = {
        'status': 200
    }
    return JsonResponse(data_return)


####################
# DIREITOS: ATESTADO
####################

def retirada_horas_atestado(request):

    # checar se requisição é autenticada
    if not request.user.is_authenticated:
        data_error = {'status': 403,
                      'content': 'Favor autenticar-se'}
        return JsonResponse(data_error)

    # data
    data = request.GET
    usuaria_id = data.get('usuaria_id', None)
    dias_atestado = data.get('dias_atestado', 0)
    data_retirada = data.get('data_retirada', '')
    if data_retirada:
        data_retirada = datetime.strptime(
            data_retirada, '%Y-%m-%d').date()

    # get pessoa
    pessoa = models.Cooperada.objects.get(pk=usuaria_id)

    # média de horas para atestado
    media_horas_atestado, media_horas_atestado_inicio, media_horas_atestado_fim = \
        pessoa.calc_media_horas_atestado(
            data_retirada, retornar_periodo=True)
    periodo_ref_calculo = f"{media_horas_atestado_inicio} até {media_horas_atestado_fim}"

    # total de horas
    horas_atestado = media_horas_atestado * Decimal(dias_atestado)

    data_return = {
        'status': 200,
        'media_horas_atestado': round(Decimal(media_horas_atestado), 2),
        'horas_atestado': round(Decimal(horas_atestado), 2),
        'periodo_ref_calculo': periodo_ref_calculo,
    }
    return JsonResponse(data_return)

def retirada_update_horas_atestado(request):

    # checar se requisição é autenticada
    if not request.user.is_authenticated:
        data_error = {'status': 403,
                      'content': 'Favor autenticar-se'}
        return JsonResponse(data_error)

    data = request.GET
    retirada_id = data.get('retirada_id', '')
    horas_atestado = data.get('horas_atestado', '')
    orcamento_atividade_atestado = data.get('orcamento_atividade_atestado', '')
    projeto_atestado = data.get('projeto_atestado', '')

    retirada = models.Retirada.objects.get(pk=retirada_id)
    atestado_obj, create = retirada.projetos.get_or_create(
        orcamento_atividade_id=orcamento_atividade_atestado,
        projeto_id=projeto_atestado,
    )
    atestado_obj.horas_trabalhadas_textual=horas_atestado
    atestado_obj.horas=horas_atestado
    atestado_obj.save()

    data_return = {
        'status': 200
    }
    return JsonResponse(data_return)


#######################
# BACKUP - VERIFICAÇÃO
#######################

class BackupView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        arr_projeto_servidor = {}
        server_list = []
        projetos_backup = models.Projeto.objects.filter(backup=True)
        for projeto in projetos_backup:
            backup_nome = projeto.backup_nome_identificacao
            backup_bucketname = projeto.backup_bucketname.nome
            backup_bucketname = backup_bucketname.replace('s3://', '')
            backup_bucketname = backup_bucketname.replace('/', '')
            backup_aws_config = projeto.backup_aws_config
            if not backup_aws_config:
                backup_aws_config='default'
            server = projeto.infraestrutura_servidor
            server_key = f"{server}*{backup_bucketname}*{backup_aws_config}"
            server_key = server_key if server else ""
            if server_key in arr_projeto_servidor:
                arr_projeto_servidor[server_key] += ','+backup_nome
            else:
                arr_projeto_servidor[server_key] = backup_nome
                server_list.append(server_key)
        content = {
            'projetos_backup': arr_projeto_servidor,
            'server_list': str(server_list).replace('[','').replace(']',''),
        }
        return Response(content)

class BackupViewOLD(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        projetos_backup = models.Projeto.objects.filter(backup=True)
        content = {'projetos_backup': ','.join([p.backup_nome_identificacao for p in projetos_backup])}
        return Response(content)


###################
# PROJECT LIST API
###################

class ProtejosAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get_beneficiarios(self, projeto):
        if projeto.beneficiario.count():
            return  [item.nome for item in projeto.beneficiario.all()]
        lista = [item.cliente.nome for item in projeto.orcamentos.all() if item.cliente]
        return list(dict.fromkeys(lista))

    def get(self, request):
        projetos = models.Projeto.objects\
            .filter(
                portfolio=True,
                imagem__isnull=False,
                encerrado=False)\
            .prefetch_related(
                'temas',
                'tecnologias',
                'produtos',
                'beneficiario',
            )\
            .order_by('?')

        projetos_return = []
        for projeto in projetos:
            projeto_dict = {
                'id': projeto.id,
                'data_insercao': projeto.data_insercao,
                'data_modificacao': projeto.data_modificacao,
                'nome': projeto.nome,
                'slug': projeto.slug,
                'descricao': projeto.descricao,
                'link': projeto.link,
                'repositorio': projeto.repositorio,
                'imagem': f"{ self.request.get_host() }{ projeto.imagem.url }" if projeto.imagem else '',
                'temas': [item.nome for item in projeto.temas.all()],
                'tecnologias': [item.nome for item in projeto.tecnologias.all()],
                'produtos': [item.nome for item in projeto.produtos.all()],
                'beneficiarios': self.get_beneficiarios(projeto),
            }
            projetos_return.append(projeto_dict)
        content = {'projetos': projetos_return}
        return Response(content)
