import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from django_summernote.admin import SummernoteModelAdmin
from django_admin_multi_select_filter.filters import (
    MultiSelectFieldListFilter, MultiSelectRelatedFieldListFilter
)

from . import forms
from .models import models
from .middleware import get_username


class BambirraAdminSite(admin.AdminSite):
    site_header = 'Bambirra'
    site_title = 'Bambirra'
    index_title = 'Administração da Cooperativa EITA!'
    enable_nav_sidebar = False

admin_site = BambirraAdminSite(name='bambirra_admin')
admin_register_options = {'site': admin_site}


class BambirraModelAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': ('css/admin/styles.css',
                    # TODO checar 'css/admin/base.css',
                    # 'css/admin/base.css',
                    )
        }

    # def get_resume_title(self, resume):
    #     try:
    #         return getattr(self, resume).short_description
    #     except:
    #         return resume.replace('_', ' ').capitalize()

    # # funcionalidade nova
    # def get_list_resume(self, request):
    #     try:
    #         list_resume = self.list_resume
    #     except:
    #         return {}
    #     return {
    #         self.get_resume_title(resume): getattr(self, resume)(request)
    #         for resume in list_resume
    #     }

    # def changelist_view(self, request, extra_context=None):
    #     extra_context = extra_context or {}
    #     extra_context['list_resume'] = self.get_list_resume(request)
    #     return super().changelist_view(request,
    #                                    extra_context=extra_context)


class CooperadaFeriasSaldoInline(admin.TabularInline):
    model = models.CooperadaFeriasSaldo
    can_delete = True
    extra=1
    ordering = ('-ano',)
    fields = ('ano', 'saldo')
    verbose_name = "Direitos: Férias"
    verbose_name_plural = "Direitos: Férias"


@admin.register(models.Cooperada, **admin_register_options)
class CooperadaAdmin(BambirraModelAdmin):
    list_display = ('__str__', 'data_entrada', 'dados', 'ano_ultima_atividade')
    search_fields = ['user__first_name', 'user__username', 'dados']
    autocomplete_fields = ('user',)
    inlines = (CooperadaFeriasSaldoInline,)
    exclude = ('data_insercao', 'data_modificacao',
               'usuario_insercao', 'usuario_modificacao')

    fields = None
    fieldsets = (
        (None, {
            'fields': (
                'user',
            )
        }),
        (None, {
            'fields': (
                'ano_ultima_atividade',
                'dados',
            )
        }),
        ('Dados da associação', {
            'fields': (
                'data_entrada',
                'data_associacao',
                'data_saida',
            )
        }),
        ('Financeiro', {
            'fields': (
                'carteira_eita',
                # 'creditos_saldo',
                )
        }),
    )

    def change_view(self, request, object_id, form_url='', extra_context=None):
        # TODO checar se precisa chamar essa atualização aqui
        # pessoa = self.get_object(request, object_id=object_id)
        # pessoa.update_ferias_saldo_ano_atual()

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


@admin.register(models.ContaBancaria, **admin_register_options)
class ContaBancariaAdmin(BambirraModelAdmin):
    list_display = ('nome', 'banco', 'titular',
                    'saldo_valor', 'saldo_atualizacao')
    search_fields = ['nome', 'banco', 'titular']
    exclude = ('data_insercao', 'data_modificacao',
               'usuario_insercao', 'usuario_modificacao',
               'saldo_ultimo_movimento_validado', 'saldo_valor_validado')


@admin.register(models.Cliente, **admin_register_options)
class ClienteAdmin(BambirraModelAdmin):
    search_fields = ['nome']


@admin.register(models.AtividadeCategoria, **admin_register_options)
class AtividadeCategoriaAdmin(BambirraModelAdmin):
    list_display = ('nome',)
    search_fields = ['nome', 'slug']
    prepopulated_fields = {"slug": ("nome",)}
    exclude = ('data_insercao', 'data_modificacao',
               'usuario_insercao', 'usuario_modificacao')


@admin.register(models.Atividade, **admin_register_options)
class AtividadeAdmin(BambirraModelAdmin):
    list_display = ('nome', 'categoria')
    search_fields = ['nome', 'slug', 'categoria__nome', 'categoria__slug']
    list_filter = ('categoria',) # TODO ordenar por nome
    prepopulated_fields = {"slug": ("nome",)}
    exclude = ('data_insercao', 'data_modificacao',
               'usuario_insercao', 'usuario_modificacao')


class AtividadeInline(admin.TabularInline):
    model = models.OrcamentoAtividade
    can_delete = True
    extra=1
    ordering = ('atividade',)
    fields = ('atividade', 'qtd_horas', 'horas_utilizadas',
            'encerramento_previsto', 'encerramento')
    autocomplete_fields = ('atividade',)


@admin.register(models.Orcamento, **admin_register_options)
class OrcamentoAdmin(BambirraModelAdmin):
    date_hierarchy = 'encerramento_previsto'
    list_display = (
            'encerramento_previsto', 'encerramento', 'projeto', 'nome', 'cliente',
            'horas', 'horas_utilizadas', 'valor',
            'inicio', )
    ordering = ('-id',)
    fields = ('nome', 'slug', 'projeto', 'cliente', 'horas', 'horas_utilizadas', 'valor', 'aprovado_em',
            'inicio', 'encerramento_previsto', 'encerramento', 'observacoes')
    autocomplete_fields = ('projeto',)
    prepopulated_fields = {"slug": ("nome",)}
    search_fields = ['nome', 'projeto__nome']
    list_filter = ('projeto',) # TODO ordenar por nome
    inlines = (AtividadeInline,)
    # list_per_page = 300 ## checar necessidade desse parâmetro


class OrcamentoInline(admin.TabularInline):
    model = models.Orcamento
    can_delete = False
    extra=0
    # ordering = ('-data_aprovavao',)
    show_change_link=True
    fields = ('id',)

@admin.action(description="Marcar selecionadas como portfolio")
def select_as_portfolio(modeladmin, request, queryset):
    queryset.update(portfolio=True)

@admin.action(description="Desmarcar selecionadas como portfolio")
def unselect_as_portfolio(modeladmin, request, queryset):
    queryset.update(portfolio=False)


@admin.register(models.Projeto, **admin_register_options)
class ProjetoAdmin(BambirraModelAdmin):
    actions = [select_as_portfolio, unselect_as_portfolio]
    form = forms.ProjetoModelForm
    # TODO verificar melhor disposição do OrcamentoInline em Projetos
    inlines = (OrcamentoInline,)
    list_display = ('nome', 'trabalho_interno', 'direito', 'portfolio',
                    'backup', 'infraestrutura_servidor',
                    'get_ultimo_orcamento_aprovado', 'encerrado',
            # 'cliente',
            # TODO descontinuar esses campo (passou direto para orçamento)
            # 'orcamento_valor', 'valor_utilizado',
            # 'inicio', 'orcamento_horas',
            # 'mes_ultima_retirada', 'horas_utilizadas_na_ultima_retirada',
            # 'horas_utilizadas', 'encerramento'
            )
    prepopulated_fields = {"slug": ("nome",)}
    search_fields = ['nome', 'slug', 'link',
                     'repositorio'] # TODO Adicionar cooperada ao filtro
    autocomplete_fields = ['beneficiario', 'tecnologias',
                           'produtos', 'temas', 'contatos']
    exclude = ('data_insercao', 'data_modificacao',
               'usuario_insercao', 'usuario_modificacao')
    list_filter = ('trabalho_interno',
                   'direito',
                   'portfolio',
                   'backup',
                   'encerrado',
                   ('tecnologias', MultiSelectRelatedFieldListFilter),
                   ('temas', MultiSelectRelatedFieldListFilter),
                   ('produtos', MultiSelectRelatedFieldListFilter),
                   'beneficiario__abrangencia',
                   'infraestrutura_servidor',
                   )

    class Media:
        js = ('js/admin/projeto/change_list.js',
              )

    @admin.display(empty_value="-", ordering="orcamentos__aprovado_em")
    def get_ultimo_orcamento_aprovado(self, obj):
        return obj.ultimo_orcamento_aprovado
    get_ultimo_orcamento_aprovado.short_description = 'Ultimo orçamento aprovado'


class MovimentoAdminBase(BambirraModelAdmin):
    # List
    date_hierarchy = 'data'
    search_fields = ['descricao',
                     'entrada__orcamento__projeto__nome',
                     'entrada__orcamento__projeto__slug']
    list_display = (
            'data',
            # 'posicao_lancamento',
            'conta', 'conta_saldo',
                    'origem', 'beneficiario', '_categoria',
                    'descricao', 'valor')
    list_display_links = ('data', 'descricao')
    list_filter = ('conta',)
    # list_resume = ('resume_valor_somado',)
    list_select_related = True
    list_per_page = 50

    # Form
    # fields = ('data_insercao', 'lancamento', 'data', 'categoria_pagamento', 'categoria_entrada',
    fields = ('lancamento', 'data', 'categoria_pagamento', 'categoria_entrada',
            'conta', 'valor_bruto', 'valor', 'tributo_retido', 'tributos_retidos', 'valor_tributo',
            'conta_saldo', 'origem', 'outra_origem', 'beneficiario', 'descricao',
            # 'quitado_em'
            )
    autocomplete_fields = ['conta']

    class Media:
        # TODO Usar Jquery local
        js = ('//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
              'js/admin/movimento/change_list.js',
              )

    # def resume_valor_somado(self, request):
    #     queryset = self.get_changelist_instance(request).queryset
    #     valor_somado = queryset.aggregate(soma=Sum('valor'))['soma']
    #     return valor_somado or 0
    # resume_valor_somado.short_description = 'Soma do valor'


@admin.register(models.Movimento, **admin_register_options)
class MovimentoAdmin(MovimentoAdminBase):
    form = forms.MovimentoModelForm


@admin.register(models.Tributo, **admin_register_options)
class TributoAdmin(BambirraModelAdmin):
    exclude = ('data_insercao', 'data_modificacao',
               'usuario_insercao', 'usuario_modificacao')


@admin.register(models.Saida, **admin_register_options)
class SaidaAdmin(MovimentoAdminBase):

    fields = ('data', 'conta',
              'conta_saldo', 'valor',
              'outra_origem', 'descricao',
              # 'quitado_em'
              )

    # def get_list_display(self, request):
    #     return self.list_display + ('quitado_em',)

    def get_model_perms(self, request):
        return {}


@admin.register(models.Entrada, **admin_register_options)
class EntradaAdmin(MovimentoAdminBase):
    # list_filter = ('projeto',) # TODO ordenar por nome
    # TODO: corrigir projetos para a busca funcionar
    # search_fields = ['projeto__nome']
    search_fields = ['descricao']
    autocomplete_fields = ('conta', 'categoria', 'projeto')
    fields = (
        'projeto', 'orcamento', 'categoria', 'data', 'conta',
        'valor_bruto', 'valor', 'valor_tributo',
        'conta_saldo', 'outra_origem', 'descricao',
    )

    # def get_fields(self, request, obj):
    #     fields = super().get_fields(request, obj)
    #     return ('projeto', 'orcamento', 'categoria') + fields

    def get_list_filter(self, request):
        list_filter = super().get_list_filter(request)
        return list_filter + ('categoria', 'projeto')

    def get_list_display(self, request):
        list_display = super().get_list_display(request)
        return list_display + ('projeto', 'orcamento', 'categoria')

    def get_model_perms(self, request):
        return {}


@admin.register(models.Pagamento, **admin_register_options)
class PagamentoAdmin(SaidaAdmin):
    search_fields = ['descricao']
    fields = ('projeto', 'categoria', 'destino',
            'descricao', 'valor', 'data', 'conta',
            # 'quitado_em'
              )
    autocomplete_fields = ('conta', 'categoria', 'projeto')

    def get_list_display(self, request):
        list_display = super().get_list_display(request)
        return list_display + ('destino', 'projeto', 'categoria')


class RetiradaProjetoInline(admin.TabularInline):
    model = models.RetiradaProjeto
    form = forms.RetiradaProjetoModelForm
    can_delete = True
    extra=3
    ordering = ('projeto',)
    fields = ('orcamento_atividade', 'projeto', 'horas_trabalhadas_textual')
    # autocomplete_fields = ('projeto',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.exclude(projeto__slug='descanso', projeto__direito=True)
        qs = qs.exclude(projeto__slug='atestado-medico', projeto__direito=True)
        return qs


@admin.register(models.Retirada, **admin_register_options)
class RetiradaAdmin(SaidaAdmin):
    # atualizar orcamentos async

    date_hierarchy = 'data_referencia'
    list_filter = ('cooperada', 'conta')
    list_display_links = (
        'data',
        # 'data_referencia',
    )
    list_display = (
        'data',
        # 'cooperada_valor_hora',
        'get_data_referencia_list', 'cooperada', 'horas_trabalhadas',
        'valor_trabalho', 'valor_ferias', 'valor'
    )
    search_fields = ['projetos__projeto__nome', 'descricao']
    inlines = (RetiradaProjetoInline,)
    # TODO ver: https://docs.djangoproject.com/en/2.2/ref/contrib/admin/#django.contrib.admin.ModelAdmin.list_select_related
    # list_select_related = True
    autocomplete_fields = ('cooperada', 'conta')
    readonly_fields = ['data',]
    form = forms.RetiradaModelForm
    fields = None
    fieldsets = (
        (None, {
            'fields': (
                'data_referencia',
                'cooperada',
                'descricao'
            )
        }),
        ('Dados do pagamento', {
            # 'classes': ('collapse',),
            'fields': (
                'conta', 'data',
                # 'quitado_em'
            )
        }),
        ('Direitos: Férias', {
            'fields': (
                'dias_ferias',
                # 'valor_ferias',
                # 'valor_trabalho',
            )
        }),
        ('Direitos: Atestado médico', {
            'fields': (
                'dias_atestado',
                'anexo_atestado',
            )
        }),
        ('Direitos: Descanso', {
            'fields': (
                'horas_descanso',
            )
        }),
    )

    @admin.display(empty_value="-", ordering="data_referencia")
    def get_data_referencia_list(self, obj):
        if not obj.data_referencia:
            return None
        return obj.data_referencia.strftime('%b, %Y')
    get_data_referencia_list.short_description = 'Mês de ref.'

    def get_data_referencia(self):
        today = datetime.date.today()
        last_month = today + relativedelta(months=-1)
        return last_month.strftime("01/%m/%Y")

    def get_form(self, request, obj=None, *args, **kwargs):
        form = super().get_form(request, *args, **kwargs)
        req = get_username()
        try:
            cooperada = req.user.cooperada
        except:
            return form
        form.base_fields['data_referencia'].initial = self.get_data_referencia()
        form.base_fields['cooperada'].initial = cooperada
        form.base_fields['conta'].initial = models.ContaBancaria.objects.get(nome__iexact='sicoob')
        if cooperada.data_associacao:
            form.base_fields['descricao'].initial = f'Pagamento retirada {cooperada}'
        else:
            form.base_fields['descricao'].initial = f'Pagamento prestação de serviço {cooperada}'
        return form

    # def get_list_display(self, request):
    #     list_display = super().get_list_display(request)
    #     return list_display + ('cooperada', 'horas_trabalhadas',) # 'cooperada_valor_hora')

    def get_model_perms(self, request):
        return super(MovimentoAdminBase, self)\
                .get_model_perms(request)

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}

        # DADOS DESCANSO
        atividade_descanso = models.OrcamentoAtividade.objects.filter(
            orcamento__projeto__slug__startswith='descanso'
        ).order_by('-orcamento__aprovado_em').first()

        extra_context['orcamento_atividade_descanso'] = atividade_descanso.id
        extra_context['projeto_descanso'] = atividade_descanso.orcamento.projeto.id

        # DADOS ATESTADO
        atividade_atestado = models.OrcamentoAtividade.objects.filter(
            orcamento__projeto__slug__startswith='atestado'
        ).order_by('-orcamento__aprovado_em').first()

        extra_context['orcamento_atividade_atestado'] = atividade_atestado.id
        extra_context['projeto_atestado'] = atividade_atestado.orcamento.projeto.id

        return super().add_view(
            request, form_url, extra_context=extra_context,
        )

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['object'] = self.get_object(request, object_id=object_id)

        # DADOS DESCANSO
        atividade_descanso = models.OrcamentoAtividade.objects.filter(
            orcamento__projeto__slug__startswith='descanso'
        ).order_by('-orcamento__aprovado_em').first()

        try:
            retirada_descanso_obj = extra_context['object'].projetos.get(
                orcamento_atividade=atividade_descanso)
            extra_context['horas_descanso'] = retirada_descanso_obj.horas
        except:
            extra_context['horas_descanso'] = ''

        extra_context['orcamento_atividade_descanso'] = atividade_descanso.id
        extra_context['projeto_descanso'] = atividade_descanso.orcamento.projeto.id

        # DADOS ATESTADO
        atividade_atestado = models.OrcamentoAtividade.objects.filter(
            orcamento__projeto__slug__startswith='atestado'
        ).order_by('-orcamento__aprovado_em').first()

        try:
            retirada_atestado_obj = extra_context['object'].projetos.get(
                orcamento_atividade=atividade_atestado)
            extra_context['horas_atestado'] = retirada_atestado_obj.horas
        except:
            extra_context['horas_atestado'] = ''

        if extra_context['horas_atestado']:
            media_horas_atestado = extra_context['object'].cooperada.calc_media_horas_atestado(extra_context['object'].data_referencia)
            extra_context['dias_atestado'] = round(Decimal(extra_context['horas_atestado']) / media_horas_atestado, 0)

        extra_context['orcamento_atividade_atestado'] = atividade_atestado.id
        extra_context['projeto_atestado'] = atividade_atestado.orcamento.projeto.id

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


@admin.register(models.OrcamentoAtividade, **admin_register_options)
class OrcamentoAtividadeAdmin(BambirraModelAdmin):
    list_display = ('orcamento', 'atividade', 'qtd_horas', 'horas_utilizadas')
    search_fields = ('atividade__nome', 'atividade__slug',
            'orcamento__projeto__nome', 'orcamento__projeto__slug')


@admin.register(models.RetiradaProjeto, **admin_register_options)
class RetiradaProjetoAdmin(BambirraModelAdmin):
    date_hierarchy = 'retirada__data'
    list_display = ('retirada', 'projeto', 'show_horas_trabalhadas_textual')
    list_filter = ('retirada__cooperada',) # , 'projeto' # TODO ordenar por nome
    autocomplete_fields = ['retirada', 'projeto', 'orcamento_atividade']
    fields = ['retirada', 'projeto', 'orcamento_atividade', 'horas_trabalhadas_textual']
    search_fields = ['projeto__nome', 'retirada__descricao',]

    def get_model_perms(self, request):
        return {}


class CooperadaInline(admin.StackedInline):
    model = models.Cooperada
    can_delete = False
    fk_name = 'user'
    exclude = ('data_insercao', 'data_modificacao',
               'usuario_insercao', 'usuario_modificacao')


admin.site.unregister(User)
@admin.register(User, **admin_register_options)
class UserAdmin(BaseUserAdmin):
    inlines = (CooperadaInline,)


@admin.register(models.Categoria, **admin_register_options)
class CategoriaAdmin(BambirraModelAdmin):
    list_display = ('nome',)
    search_fields = ['nome',]


@admin.register(models.CategoriaPagamento, **admin_register_options)
class CategoriaPagamentoAdmin(CategoriaAdmin):
    pass


@admin.register(models.CategoriaEntrada, **admin_register_options)
class CategoriaEntradaAdmin(CategoriaAdmin):
    pass


@admin.register(models.Tecnologia, **admin_register_options)
class TecnologiaAdmin(CategoriaAdmin):
    pass


@admin.register(models.TipoProduto, **admin_register_options)
class TipoProdutoAdmin(CategoriaAdmin):
    pass


@admin.register(models.Tema, **admin_register_options)
class TemaAdmin(CategoriaAdmin):
    pass


@admin.register(models.InfraestruturaServidor, **admin_register_options)
class InfraestruturaServidorAdmin(CategoriaAdmin):
    list_display = ('nome', 'get_trabalhos',)
    search_fields = ['nome', 'trabalhos__nome',]
    list_filter = ('trabalhos__nome',)

    @admin.display(empty_value="-", ordering="trabalhos__nome")
    def get_trabalhos(self, obj):
        trabalhos = [t.nome for t in obj.trabalhos.all()]
        return ', '.join(trabalhos)
    get_trabalhos.short_description = 'Trabalhos alocados'


@admin.register(models.BackupBucketName, **admin_register_options)
class BackupBucketNameAdmin(CategoriaAdmin):
    list_display = ('nome', 'get_trabalhos',)
    search_fields = ['nome', 'trabalhos__nome',]
    list_filter = ('trabalhos__nome',)

    @admin.display(empty_value="-", ordering="trabalhos__nome")
    def get_trabalhos(self, obj):
        trabalhos = [t.nome for t in obj.trabalhos.all()]
        return ', '.join(trabalhos)
    get_trabalhos.short_description = 'Trabalhos alocados'


@admin.register(models.Pais, **admin_register_options)
class PaisAdmin(CategoriaAdmin):
    pass


@admin.register(models.UF, **admin_register_options)
class UFAdmin(CategoriaAdmin):
    pass

@admin.register(models.Municipio, **admin_register_options)
class MunicipioAdmin(CategoriaAdmin):
    pass


class ClientesComContato(admin.SimpleListFilter):
    title = "Clientes"

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "clientes"

    def lookups(self, request, model_admin):
        clientes = models.Cliente.objects.exclude(contatos=None)
        return [(cliente.nome, cliente.nome) for cliente in clientes]

    def queryset(self, request, queryset):
        return models.Contato.objects.filter(clientes__nome=self.value());


class TrabalhosComContato(admin.SimpleListFilter):
    title = "Trabalhos"

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "projetos"

    def lookups(self, request, model_admin):
        projetos = models.Projeto.objects.exclude(contatos=None)
        return [(projeto.nome, projeto.nome) for projeto in projetos]

    def queryset(self, request, queryset):
        if 'clientes' in request.GET:
            if self.value():
                return models.Contato.objects.filter(projetos__nome=self.value()).filter(clientes__nome=request.GET['clientes'])
            else:
                return models.Contato.objects.filter(clientes__nome=request.GET['clientes'])
        else:
            return models.Contato.objects.filter(projetos__nome=self.value())


class ContatoClienteInline(admin.TabularInline):
    model = models.Cliente.contatos.through


class ContatoProjetoInline(admin.TabularInline):
    model = models.Projeto.contatos.through


@admin.register(models.Contato, **admin_register_options)
class ContatoAdmin(CategoriaAdmin):
    form = forms.ContatoModelForm
    list_display = ('nome', 'telefone', 'email', 'clientes_relacionados', 'trabalhos')
    list_filter = (ClientesComContato,TrabalhosComContato)

    inlines = [
        ContatoClienteInline,
        ContatoProjetoInline,
    ]


@admin.register(models.Reuniao, **admin_register_options)
class ReuniaoAdmin(BambirraModelAdmin, SummernoteModelAdmin):
    summernote_fields = '__all__'
    list_display = ('data',
                    'titulo',
                    'get_orcamento',
                    'get_participantes',
                    )
    list_filter = ('participantes', 'orcamento__projeto',)
    search_fields = ['titulo', 'orcamento__projeto__nome', 'orcamento__projeto__slug',
                     'anotacoes', 'orcamento__nome', 'orcamento__slug']
    date_hierarchy = 'data'
    autocomplete_fields = ['orcamento',]
    filter_horizontal = ('participantes',)

    @admin.display(empty_value="-", ordering="orcamento__nome")
    def get_orcamento(self, obj):
        if not obj.orcamento:
            return '-'
        orcamento = str(obj.orcamento)
        if len(orcamento) > 50:
            return orcamento[:50] + '...'
        else:
            return orcamento
    get_orcamento.short_description = 'Orçamento'

    @admin.display(empty_value="-", ordering="participantes__user")
    def get_participantes(self, obj):
        return ', '.join([str(x) for x in obj.participantes.all()])
    get_participantes.short_description = 'Participantes'

    # class Media:
    #    # TODO Usar Jquery local
    #    js = ('//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
    #          'js/admin/reuniao/change_form.js',
    #          )
