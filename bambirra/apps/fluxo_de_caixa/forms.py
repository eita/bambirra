from datetime import datetime
from decimal import Decimal

from dal import autocomplete

from django import forms
from django.core.exceptions import ValidationError
from django.contrib.admin.widgets import AdminDateWidget

from .models import models


class RetiradaModelForm(forms.ModelForm):

    dias_atestado = forms.IntegerField(
        required=False, label='Qtde. dias de atestado',
        help_text='&nbsp;'
    )

    horas_descanso = forms.DecimalField(
        required=False, label='Horas de descanso',
        help_text='Cálculo das horas de descanso',
        max_digits=5,
        decimal_places=2
    )

    class Meta:
        widgets = {
            'horas_iteraveis': forms.Textarea(attrs={'cols': 90, 'rows': 4}),
        }

    def clean(self):
        cleaned_data = super().clean()
        anexo_atestado_data = cleaned_data.get("anexo_atestado")
        dias_atestado_data = cleaned_data.get("dias_atestado")
        if not dias_atestado_data and anexo_atestado_data:
            self.add_error("dias_atestado", "Informe o número de dias que ficou de atestado médico.")
            self.add_error("anexo_atestado", "Selecione novamente o anexo.")

    def save(self, commit=True):

        instance = super(RetiradaModelForm, self).save(commit=False)

        if not instance.id:
            return instance

        if 'data_referencia' not in self.changed_data:
            return instance

        if commit:
            instance.save()

        instance.get_cooperada_valor_hora_retirada(recalcular=True)

        return instance


class RetiradaProjetoModelForm(forms.ModelForm):

    class Meta:
        widgets = {
            'projeto': forms.HiddenInput(),
            'orcamento_atividade': autocomplete.ModelSelect2(
                url='trabalho_atividade-ativo-autocomplete'),
        }
        labels = {
            'orcamento_atividade': 'trabalho',
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        orcamento_atividade = cleaned_data.get("orcamento_atividade")

        cleaned_data['projeto'] = orcamento_atividade.orcamento.projeto

        return cleaned_data


class MovimentoModelForm(forms.ModelForm):
    lancamento = forms.IntegerField(required=False,
            widget=forms.HiddenInput())
    conta_saldo = forms.DecimalField(required=False,
            widget=forms.HiddenInput())
    valor = forms.DecimalField(label='Valor líquido',
                               required=False)
    categoria_entrada = forms.ModelChoiceField(
        required=False,
        queryset=models.CategoriaEntrada.objects.all(),
        widget=autocomplete.ModelSelect2(
            # TODO set create_field='nome' on urls autocomplete
            url='categoria_entrada-autocomplete'))
    categoria_pagamento = forms.ModelChoiceField(
        required=False,
        queryset=models.CategoriaPagamento.objects.all(),
        widget=autocomplete.ModelSelect2(
            # TODO set create_field='nome' on urls autocomplete
            url='categoria_pagamento-autocomplete'))
    # conta = forms.ModelChoiceField(
    #     required=False,
    #     queryset=models.ContaBancaria.objects.all(),
    #     widget=autocomplete.ModelSelect2(
    #         # TODO set create_field='nome' on urls autocomplete
    #         url='conta-bancaria-autocomplete'))

    tributo_retido = forms.BooleanField(required=False)
    tributos_retidos = forms.ModelMultipleChoiceField(
        label='Tributos',
        queryset=models.Tributo.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False)
    origem = forms.ModelChoiceField(
        required=False,
        queryset=models.Orcamento.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='orcamento-autocomplete'))
    beneficiario = forms.CharField(
        required=False, label='Beneficiário',
        help_text='No caso de recebimentos, será atribuída "EITA" \
        caso nenhum beneficiário seja informado'
    )
    quitado_em = forms.DateField(
        required=False,
        widget=AdminDateWidget)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Retirada a obrigatoriedade da data do movimento em 23/01/2024
        # self.fields['data'].required = True
        self.fields['valor_bruto'].required = True
        self.fields['valor'].widget.attrs['readonly'] = True

        # conta
        conta = None
        try:
            if not self.instance.conta:
                conta = models.ContaBancaria.objects.get(nome__iexact='sicoob')
        except:
            conta = None
        self.fields['conta'].initial = conta

        # PREPARE INITIAL VALUES
        self.class_name = ''
        if self.instance.id:
            self.instance = models.Movimento.objects.filter(
                id=self.instance.id).select_subclasses()[0]
            self.class_name = self.instance.__class__.__name__
            # TODO verificar quando for retirada

            self.fields['lancamento'].initial = self.instance.id

            if 'Entrada' in self.class_name:
                self.fields['categoria_entrada'].initial = self.instance.categoria
            else:
                self.fields['quitado_em'].initial = self.instance.quitado_em
                try:
                    self.fields['categoria_pagamento'].initial = self.instance.categoria
                except:
                    pass

            if self.instance.tributos.count():
                self.fields['tributo_retido'].initial = True
                self.fields['tributos_retidos'].initial = self.instance.tributos.all()

            if self.instance.outra_origem:
                self.fields['outra_origem'].initial = self.instance.outra_origem
            else:
                try:
                    self.fields['origem'].initial = self.instance.orcamento
                except:
                    pass

    def clean(self):
        cleaned_data = self.cleaned_data

        valor = cleaned_data.get('valor', 0)
        categoria_entrada = cleaned_data.get('categoria_entrada', None)
        categoria_pagamento  = cleaned_data.get('categoria_pagamento', None)
        origem  = cleaned_data.get('origem', None)
        outra_origem  = cleaned_data.get('outra_origem', '')
        if outra_origem: outra_origem = outra_origem.strip()

        if categoria_entrada and categoria_pagamento:
            self.add_error('categoria_entrada', "Selecione a categoria relacionada ao tipo do lançamento.")

        if categoria_entrada and valor < 0:
            self.add_error('valor_bruto', "O valor para lançamento de Entrada deve ser positivo.")
        if categoria_pagamento and valor > 0:
            self.add_error('valor_bruto', "O valor para lançamento de Pagamento deve ser negativo.")

        if origem and outra_origem:
            self.add_error('origem', "Selecione uma ou outra origem.")

        if origem and not categoria_entrada and not categoria_pagamento:
            self.add_error('categoria_entrada', "A origem deve estar relacionada a uma das categorias.")

        # validar: origem required quando Entrada
        # if categoria_entrada and not origem:
            # self.add_error('origem', "Você deve selecionar uma origem para este tipo de lançamento.")

        return cleaned_data

    def save_movimento(self):
        data = self.cleaned_data

        valor = data.get('valor', 0)
        valor_bruto = data.get('valor_bruto', 0)

        # ENTRADA
        if valor_bruto > 0:
            if not self.instance.id:
                model_instance = models.Entrada()
            else:
                model_instance = self.instance
                # checar se é uma alteração: de Entrada para uma Saída
                if 'Entrada' not in str(model_instance):
                    model_instance.delete()
                    model_instance = models.Entrada()
            model_instance.categoria = data.get('categoria_entrada', None)

        # SAÍDA
        else:
            categoria_pagamento = data.get('categoria_pagamento', None)
            if categoria_pagamento:
                if not self.instance.id:
                    model_instance = models.Pagamento()
                else:
                    model_instance = self.instance
                    # checar se é uma alteração: de Entrada para uma Saída
                    if 'Pagamento' not in str(model_instance):
                        model_instance.delete()
                        model_instance = models.Pagamento()

                model_instance.destino = data.get('beneficiario', '')
                model_instance.categoria = categoria_pagamento
            else:
                if not self.instance.id:
                    model_instance = models.Saida()
                else:
                    model_instance = self.instance
                    # if 'Saida' not in str(model_instance):
                    #     model_instance.delete()
                    #     model_instance = models.Saida()

            model_instance.outra_origem = data.get('outra_origem', 'EITA')
            model_instance.quitado_em = data.get('quitado_em')

        # ATRIBUTOS COMUNS
        model_instance.valor = valor
        model_instance.valor_bruto = valor_bruto
        model_instance.valor_tributo = data.get('valor_tributo', '')
        model_instance.descricao = data.get('descricao', '')
        origem = data.get('origem', None)
        if origem: model_instance.orcamento = origem
        model_instance.data = data.get('data')

        conta = data.get('conta', None)
        if conta:
            try:
                model_instance.conta = models.ContaBancaria.objects.get(
                    id=conta.id)
            except:
                raise
            model_instance.conta_saldo = data.get('conta_saldo', '')

        # SALVAR LANÇAMENTO
        try:
            instance = model_instance.save()
            self._save_m2m()
            self.save_m2m = self._save_m2m
        except:
            # import pdb; pdb.set_trace()
            raise

        # ATUALIZA TRIBUTOS
        if 'tributos_retidos' in self.changed_data:
            # import pdb; pdb.set_trace()
            model_instance.atualiza_tributos(
                    [item.id for item in data.get('tributos_retidos')])

        # ATUALIZAR SALDOS
        if conta:
            conta.atualizar_saldo_lancamentos()

        return model_instance

    def save(self, commit=True):
        if self.errors:
            raise ValueError(
                "The %s could not be %s because the data didn't validate." % (
                    self.instance._meta.object_name,
                    'created' if self.instance._state.adding else 'changed',
                )
            )

        model_instance = self.save_movimento()
        try:
            return model_instance.movimento_ptr
        except:
            return model_instance


class ProjetoModelForm( forms.ModelForm ):
    class Meta:
        widgets={'descricao': forms.Textarea }


class ContatoModelForm( forms.ModelForm ):
    class Meta:
        widgets={'observacoes': forms.Textarea }
