import urllib

from django.views.generic import ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q

from .models import models
from .admin import ProjetoAdmin


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class PortfolioListView(LoginRequiredMixin, ListView):
    model = models.Projeto

    def get_queryset(self):
        qs = super().get_queryset()

        querystring = self.request.META.get('QUERY_STRING', '')
        querystring = urllib.parse.unquote(querystring)
        querystring = querystring.split('&')

        dict_criteria = {}
        for item_filter in querystring:
            criteria = item_filter.split('=')
            if not ''.join(criteria):
                criteria = ['portfolio', True]
            valor = criteria[1]
            criteria_key = criteria[0]

            # ignore page number on querystring
            if criteria_key == 'p':
                continue

            if criteria_key == 'q':
                criteria_search_field = []
                for search_field in ProjetoAdmin.search_fields:
                    criteria_search_field.append(f'Q({search_field}__icontains="{valor}")')
                qs = qs.filter(eval(' | '.join(criteria_search_field)))
            else:
                if '__in' in criteria_key:
                    valor = eval(f'({valor},)')
                else:
                    valor = eval(f'({valor})')
                dict_criteria.update({criteria_key: valor})

        dict_criteria['encerrado'] = False

        qs = qs\
            .filter(**dict_criteria)\
            .distinct()

        return qs
