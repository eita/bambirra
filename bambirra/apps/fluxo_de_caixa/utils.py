import re, csv
from openpyxl import Workbook
from openpyxl.styles import Alignment, Font, Border, Side, PatternFill

from django.conf import settings
from fluxo_de_caixa.models import models
from datetime import datetime

def gerar_livro_caixa(month, year):
    # media_root / livro-caixa-2022-08.xls
    filename = f"livro-caixa-{ year }-{month}.xls"
    export_path = f"{ settings.MEDIA_ROOT }/{ filename }"
    media_url = f"{ settings.MEDIA_URL }{ filename }"

    thin_border = Border(left=Side(style='thin'),
                 right=Side(style='thin'),
                 top=Side(style='thin'),
                 bottom=Side(style='thin'))

    workbook = Workbook()

    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = 'Livro Caixa ' + month + ' - ' + year

    columns = [
        'Data',
        'Item',
        'Entrada',
        'Saida',
        'Saldo',
    ]

    movimento_queryset = models.Movimento.objects\
            .filter(data__year=year, data__month=month, conta__nome='Sicoob')\
            .order_by('data', 'posicao_lancamento').all()

    saldo_anterior = models.Movimento.objects\
            .filter(data__lt=movimento_queryset.first().data,
                    conta__nome='Sicoob')\
            .order_by('-data', '-posicao_lancamento').first().conta_saldo

    row_num = 6
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title
        cell.alignment = Alignment(horizontal='center')
        cell.font = Font(name='Arial', bold=True)
        cell.border = thin_border

    soma_entrada = 0
    soma_saida = 0
    ultimo_saldo = 0

    for movimento in movimento_queryset:
        row_num += 1

        entrada = movimento.valor if movimento.valor >= 0 else ''
        saida = abs(movimento.valor) if movimento.valor < 0 else ''

        soma_entrada += movimento.valor if movimento.valor >= 0 else 0
        soma_saida += abs(movimento.valor) if movimento.valor < 0 else 0
        ultimo_saldo = movimento.conta_saldo

        # Define the data for each cell in the row
        row = [
            movimento.data.strftime("%d/%m/%Y"),
            movimento.descricao,
            entrada,
            saida,
            movimento.conta_saldo,
        ]

    # Assign the data for each cell of the row
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            if col_num in [3,4,5]:
                cell.alignment = Alignment(horizontal='right')
                cell.number_format = 'R$ #,##0.00'
            cell.value = cell_value
            cell.border = thin_border

    row_num += 1
    cell = worksheet.cell(row=row_num, column=3)
    cell.value = soma_entrada
    cell.border = thin_border
    cell.fill = PatternFill(start_color="99CC00", end_color="99CC00", fill_type = "solid")
    cell.number_format = 'R$ #,##0.00'

    cell = worksheet.cell(row=row_num, column=4)
    cell.value = soma_saida
    cell.border = thin_border
    cell.fill = PatternFill(start_color="99CC00", end_color="99CC00", fill_type = "solid")
    cell.number_format = 'R$ #,##0.00'

    cell = worksheet.cell(row=row_num, column=5)
    cell.value = ultimo_saldo
    cell.border = thin_border
    cell.fill = PatternFill(start_color="99CC00", end_color="99CC00", fill_type = "solid")
    cell.number_format = 'R$ #,##0.00'

    row_num = row_num + 1

    cell = worksheet.cell(row=row_num, column=2)
    cell.value = 'Total Entrada (acumulado)'
    cell.border = thin_border

    row_num = row_num + 1

    cell = worksheet.cell(row=row_num, column=2)
    cell.value = 'Total de despesas cooperativa (acumulado)'
    cell.border = thin_border


    adjust_columns_size(worksheet)

    cell = worksheet.cell(row=1, column=1)
    cell.value = 'EITA – Cooperativa de Trabalho Educação, Informação e Tecnologia para Autogestão'
    cell.alignment = Alignment(horizontal='center')
    cell.font = Font(name='Arial', bold=True)
    cell.border = thin_border

    cell = worksheet.cell(row=2, column=1)
    cell.value = 'CNPJ: 19515323/0001-46           NIRE: 43400101969                   de 13.01.2014'
    cell.alignment = Alignment(horizontal='center')
    cell.border = thin_border

    cell = worksheet.cell(row=3, column=1)
    cell.value = 'Rua Sabato Generoso, Nº 202 - Centro Caldas/MG - CEP 37780-000'
    cell.alignment = Alignment(horizontal='center')
    cell.border = thin_border

    cell = worksheet.cell(row=5, column=4)
    cell.value = 'Saldo Anterior'
    cell.alignment = Alignment(horizontal='center')
    cell.border = thin_border

    cell = worksheet.cell(row=5, column=5)
    cell.value = saldo_anterior
    cell.alignment = Alignment(horizontal='right')
    cell.border = thin_border
    cell.number_format = 'R$ #,##0.00'

    worksheet.merge_cells(start_row=1, start_column=1, end_row=1, end_column=5)
    worksheet.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
    worksheet.merge_cells(start_row=3, start_column=1, end_row=3, end_column=5)
    worksheet.merge_cells(start_row=4, start_column=1, end_row=4, end_column=5)

    workbook.save(export_path)

    return media_url

def format_decimal(value):
    return "{:,.2f}".format(value).replace(",", "*").replace(".", ",").replace("*", ".")

def adjust_columns_size(worksheet):
    for col in worksheet.columns:
        max_length = 0
        column = col[0].column_letter # Get the column name
        for cell in col:
            try: # Necessary to avoid error on empty cells
                if len(str(cell.value)) > max_length:
                    max_length = len(str(cell.value))
            except:
                pass
        adjusted_width = (max_length + 2) * 1.4
        worksheet.column_dimensions[column].width = adjusted_width
