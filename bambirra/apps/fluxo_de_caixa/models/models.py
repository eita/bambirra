import os
import re
from decimal import Decimal
from datetime import datetime, date
from dateutil.relativedelta import relativedelta

from model_utils.managers import InheritanceManager

from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.utils import IntegrityError
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.html import format_html
from django.db import models
from django.db.models import Sum, Q

from .base import Base


class Pais(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']
        verbose_name_plural = "Países"


class UF(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)
    sigla = models.CharField(max_length=2, null=True, blank=False)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']
        verbose_name_plural = "UFs"


class Municipio(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)
    uf = models.ForeignKey(UF, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.nome} ({self.uf.sigla})"

    class Meta:
        ordering = ['nome']
        verbose_name_plural = "Municípios"


class Contato(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)
    telefone = models.CharField(max_length=100, null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    observacoes = models.CharField(max_length=100,
        null=True, blank=True, verbose_name="Observações")

    def __str__(self):
        if self.email and self.telefone:
            return f"{self.nome} ({self.telefone}, {self.email})"
        elif self.email and not self.telefone:
            return f"{self.nome} ({self.email})"
        elif not self.email and self.telefone:
            return f"{self.nome} ({self.telefone})"
        else:
            return f"{self.nome}"

    def clientes_relacionados(self):
        return ", ".join([c.nome for c in self.clientes.all()])

    def trabalhos(self):
        return ", ".join([c.nome for c in self.projetos.all()])

    class Meta:
        ordering = ['nome']


class Cliente(Base):
    """
    Esta classe Cliente se relaciona com Orcamento a título informativo e de filtro.
    """

    class Abrangencia(models.TextChoices):
        NACIONAL = 'Na', 'Nacional'
        REGIONAL = 'Re', 'Regional'
        ESTADUAL = 'Es', 'Estadual'
        MUNICIPAL = 'Mu', 'Municipal'

    nome = models.CharField(max_length=100, null=True, blank=True)
    slug = models.SlugField(null=True, unique=True)
    # campo legado - #71
    contato = models.CharField(max_length=200, null=True, blank=True)
    abrangencia = models.CharField(
        max_length=2, null=True, blank=True, choices=Abrangencia.choices)
    pais = models.ForeignKey(Pais,
        on_delete=models.SET_NULL, null=True, blank=True)
    UF = models.ForeignKey(UF,
        on_delete=models.SET_NULL, null=True, blank=True)
    municipio = models.ForeignKey(Municipio,
        on_delete=models.SET_NULL, null=True, blank=True)
    contatos = models.ManyToManyField(Contato,
        related_name='clientes', blank=True)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome',]


class ContaBancaria(Base):
    """
    Registra a conta e o saldo consolidado
    a título de recalculos no movimento financeiro
    """

    nome = models.CharField(
        max_length=100, null=True, blank=False, )
    banco = models.CharField(
        max_length=100, null=True, blank=True, )
    titular = models.CharField(
        max_length=100, null=True, blank=True, )

    saldo_valor = models.DecimalField(
        'saldo consolidado',
        max_digits=10, decimal_places=2,
        blank=True, null=True, default=0)
    saldo_atualizacao = models.DateField(
        'saldo consolidado em', null=True, blank=True,
        default=datetime.now)

    data_ultimo_lancamento = models.DateField(
            'Data do último lançamento relacionado',
            null=True, blank=True)

    anotacoes = models.TextField(
            'anotações',
            null=True, blank=True)

    """
    TODO desconsitnuar campos de ContaBancaria
    Os campos abaixo controlavam o recalculo do saldo
    quando era feito integralmente quando necessário,
    ex.: em registro e atualização do movimento financeiro.
    O método de recalculo foi modificado e o saldos são registrados
    linha a linha no Movimento.
    """
    # Este campo foi descontinuado pois saldo_valor já atende
    saldo_ultimo_movimento_validado = models.PositiveIntegerField(
        null=True, blank=True)
    # Este campo foi descontinuado pois saldo_valor já atende
    # o controle de saldo válido.
    saldo_valor_validado = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)

    def validar_saldo(self):
        ultimo_movimento = self.movimentos.all().order_by('-posicao_lancamento').first()

        if not ultimo_movimento:
            print(f'{self} - validação do saldo: Não há movimento para validar o saldo')
            return None

        self.saldo_ultimo_movimento_validado = ultimo_movimento.id
        self.saldo_valor_validado = self.saldo_valor
        self.save()

    def atualizar_saldo(self):

        # TODO refatorar calculo, utilizando data movimento
        return None

    def atualizar_saldo_lancamentos(self):

        saldo_anterior = self.saldo_valor
        saldo_calculado = 0

        # Obter Movimentos relacionados a conta com data maior que ultima atualização de saldo
        lancamentos = Movimento.objects\
                .filter(conta=self,
                        data__gt=self.saldo_atualizacao)\
                                .order_by('data', 'posicao_lancamento')
        for lancamento in lancamentos:
            if not saldo_calculado:
                saldo_calculado = saldo_anterior + lancamento.valor
            else:
                saldo_calculado = saldo_calculado + lancamento.valor

            # Atualizar registro de saldo conforme valor do lançamento
            lancamento.conta_saldo = saldo_calculado
            lancamento.save(atualizar_saldo=False)

        return None

    def atualizar_saldo_original(self):

        # TODO refatorar calculo, utilizando data movimento
        return None

        if not self.saldo_ultimo_movimento_validado:
            return None

        movimentos = self.movimentos.filter(
            id__gt=self.saldo_ultimo_movimento_validado).order_by('data')

        if not movimentos:
            self.save()
            return None

        self.saldo_valor = self.saldo_valor_validado

        for movimento in movimentos:
            valor = movimento.valor
            if valor == None:
                valor = 0

            self.saldo_valor += valor

        self.saldo_atualizacao = movimentos.last().data

        self.save()

    def consolidar_saldo(self):
        # se a diferença de anos entre saldo_atualizacao e data_ultimo_lancamento
        # for maior que 1 ano, consolidar conta com ano da  data_ultimo_lancamento menos um.

        if (not self.data_ultimo_lancamento
                or not self.saldo_atualizacao):
            return

        ano_ultimo_lancamento = self.data_ultimo_lancamento.year
        saldo_atualizacao_diff = ano_ultimo_lancamento - self.saldo_atualizacao.year
        if saldo_atualizacao_diff > 1:
            ano_saldo_atualizacao = ano_ultimo_lancamento-1
            self.saldo_atualizacao =datetime.strptime(f"{ano_saldo_atualizacao}-01-01" , '%Y-%m-%d')

            # atualizar valor consolidado
            saldo_atualizacao_valor = self.movimentos\
                .filter(data__lte=self.saldo_atualizacao)\
                .order_by('-data', '-posicao_lancamento')\
                .values('conta_saldo')[0]['conta_saldo']
            self.saldo_valor = saldo_atualizacao_valor
            self.save()

    def save(self):
        super(ContaBancaria, self).save()
        if not self.data_ultimo_lancamento:
            self.data_ultimo_lancamento = \
                    date.today()
        super(ContaBancaria, self).save()

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        # ordering = ['-saldo_atualizacao', 'nome']
        ordering = ['-data_ultimo_lancamento', 'nome']
        verbose_name = "conta bancária"
        verbose_name_plural = "contas bancárias"


class Cooperada(Base):
    """
    Esta entidade é utilizada:
    * gerenciamento das atividades de orçamentos
    * movimentações financeiras.
    """
    user = models.OneToOneField(
        User,
        verbose_name='Conta de usuária',
        on_delete=models.CASCADE,
        null=True, blank=False)
    data_entrada = models.DateField('data de entrada', null=True, blank=True)

    # Este campo define se é uma Cooperada ou prestadora de serviços
    data_associacao = models.DateField('data de associação', null=True, blank=True)

    data_saida = models.DateField('data de saída', null=True, blank=True)

    dados = models.TextField(
        null=True, blank=True,
        help_text='id, cpf, conta, endereço, telefone, ...')

    carteira_eita = models.ForeignKey(
        ContaBancaria, on_delete=models.SET_NULL,
        null=True, blank=True,
        related_name='cooperadas')

    # TODO Cooperada creditos - descontinuar "creditos_saldo" e tratar na ContaBancaria
    # creditos_saldo = models.DecimalField(
    #     max_digits=10, decimal_places=2,
    #     blank=True, null=True)

    # def get_ano_ultima_atividade_default():
    #     return int(datetime.today().strftime('%Y'))

    ano_ultima_atividade = models.PositiveSmallIntegerField(
            # default=get_ano_ultima_atividade_default,
            null=True, blank=True)

    def __str__(self):
        user = self.user
        verbose_name = f"{user.first_name} {user.last_name}".strip()
        if verbose_name:
            return verbose_name

        return f"{self.user.username}"

    class Meta:
        ordering = ['-ano_ultima_atividade', 'user__username']
        verbose_name = "pessoa"
        verbose_name_plural = "pessoas"

    def save(self):
        super(Cooperada, self).save()
        self.ano_ultima_atividade = \
                self.get_ano_ultima_atividade()
        super(Cooperada, self).save()

    def get_ano_ultima_atividade(self):
        ultima_atividade = self.retiradas\
                .order_by('-data', '-data_insercao')\
                .first()
        if not ultima_atividade:
            data = self.data_entrada
            if not data:
                return 2012
        else:
            data = ultima_atividade.data
            if not data:
                data = ultima_atividade.data_insercao
        return data.year

    def update_ano_ultima_atividade(self):
        self.ano_ultima_atividade = \
                self.get_ano_ultima_atividade()
        self.save()

    def update_ferias_saldo_ano_atual(self):
        # verifica se tem o registro de saldo do ano atual.
        # Se não tiver:
        # verifica se pessoa:
        # * tem atividades recentes
        # * tem pelo menos um ano
        # OU ...
        ferias_saldo_atual, created = self.ferias_saldo.get_or_create(
            ano=datetime.today().year
        )
        if created:
            ferias_saldo_atual.saldo=30
            ferias_saldo_atual.save()

    def get_media_horas_ferias_periodo(self, data=None, verbose_format=False):

        if not data:
            data = datetime.today()

        ano_atual = data.year
        ultimo_ano = ano_atual - 1

        if verbose_format:
            periodo_inicio = f'15/01/{ultimo_ano}'
            periodo_fim = f'15/01/{ano_atual}'
        else:
            periodo_inicio = f'{ultimo_ano}-01-15'
            periodo_fim = f'{ano_atual}-01-15'

        return periodo_inicio, periodo_fim

    def calc_media_horas_ferias(self, data=None):
        """
        Férias: média no ano de anterior (por ano) com descanso
        """

        periodo_inicio, periodo_fim = \
            self.get_media_horas_ferias_periodo(data)

        # Somar horas_trabalhadas de retiradas do ultimo ano, de jan - dez
        retiradas = self.retiradas.filter(
            Q(data_referencia__gte=periodo_inicio) & \
            Q(data_referencia__lte=periodo_fim)
        )

        if not retiradas:
            return 0

        total_horas_trabalhadas = \
            retiradas.aggregate(
                Sum('horas_trabalhadas'))['horas_trabalhadas__sum']

        media_horas = total_horas_trabalhadas / 365

        return media_horas

    def calc_media_horas_atestado(self, data=None, retornar_periodo=False):

        """
        Regra:

        * Somar horas_trabalhadas de retiradas dos ultimos 12 meses
        """

        if not data:
            data = datetime.now().date()

        mes_atual = data.replace(day=15)

        data_final = mes_atual
        if data.day <= 15:
            data_final = data_final + relativedelta(months=-1)

        data_inicial = data_final + relativedelta(months=-12)

        # Somar horas_trabalhadas de retiradas dos ultimos 12 meses
        retiradas = self.retiradas.filter(
            data_referencia__gte=data_inicial,
            data_referencia__lte=data_final
            # ).order_by('data').values('id', 'data')
        ).order_by('data_referencia').values('data_referencia', 'id', 'data')

        if not retiradas:
            return 0

        projetos_retiradas = RetiradaProjeto.objects\
            .filter(retirada_id__in=[r['id'] for r in retiradas])

        total_horas_trabalhadas = \
                projetos_retiradas.aggregate(
                            Sum('horas'))['horas__sum']

        # Diminui um mês para garantir a contabilização de dias da primeira retirada.
        # mes_primeira_retirada_periodo = retiradas.first()['data_referencia'] + relativedelta(months=-1)
        mes_primeira_retirada_periodo = retiradas.first()['data_referencia']
        mes_primeira_retirada_periodo = mes_primeira_retirada_periodo.replace(day=15)
        delta = data_final - mes_primeira_retirada_periodo
        delta = delta.days

        media_horas = total_horas_trabalhadas / delta

        if retornar_periodo:
            return \
                media_horas, \
                data_inicial.strftime('%d/%m/%Y'), \
                data_final.strftime('%d/%m/%Y')
        else:
            return media_horas


class CooperadaFeriasSaldo(Base):

    # No CooperadaFeriasSaldo terá o saldo por ano, somente;

    def current_year():
        return date.today().year

    def max_value_current_year(value):
        return MaxValueValidator(date.today().year)(value)

    cooperada = models.ForeignKey(
        Cooperada, on_delete=models.CASCADE,
        related_name='ferias_saldo',
        verbose_name='Trabalhador(a)',
        null=True, blank=False)
    ano = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2012), max_value_current_year])
    saldo = models.PositiveSmallIntegerField(
        'saldo de dias',
        null=True, blank=False,
        validators=[MaxValueValidator(30)])

    class Meta:
        unique_together = ['cooperada', 'ano']
        verbose_name = "Direitos: Férias"
        verbose_name_plural = "Direitos: Férias"
        ordering = ['cooperada', 'ano']

    def __str__(self):
        if self.saldo:
            return f"{self.cooperada} - Férias {self.ano}: Saldo de {self.saldo} dias"
        else:
            return f"{self.cooperada} - Férias {self.ano}: Cumprida!"

    def get_valor_hora(self):
        historico = settings.COOPERADA_VALOR_HORA_HISTORICO
        # ordenar crescente pela data de vigência do valor/hora
        historico = dict(sorted(historico.items()))
        for data in historico:
            ano, mes, dia = data.split('-')
            if str(self.ano) in data:
                if int(mes) <= 6:
                    valor_hora = historico[data]
                break
            else:
                if int(ano) < self.ano:
                    valor_hora = historico[data]
        return valor_hora


class Tecnologia(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']


class TipoProduto(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']


class Tema(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']


class InfraestruturaServidor(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']
        verbose_name = "Servidor (infraestrutura)"
        verbose_name_plural = "Servidores (infraestrutura)"


class BackupBucketName(models.Model):
    nome = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']
        verbose_name = "Backup S3 Bucketname"
        verbose_name_plural = "Backup S3 Bucketnames"


class Projeto(Base):
    trabalho_interno = models.BooleanField(
        null=False, blank=False, default=False)
    direito = models.BooleanField(
        null=False, blank=False, default=False)
    nome = models.CharField(
        max_length=100, null=False, blank=False,
        unique=True)
    slug = models.SlugField(null=True, unique=True)
    beneficiario = models.ManyToManyField(Cliente, related_name='projetos', blank=True,
        help_text="Caso o trabalho tenha sido feito para uma organização diferente daquela que pagou o orçamento, \
        escolha a organização. Deixe em branco caso seja a mesma organização do Orçamento")
    descricao = models.CharField('descrição', max_length=500, null=True, blank=True)
    # cliente = models.ForeignKey(
    #     Cliente, on_delete=models.CASCADE,
    #     null=True, blank=True)
    portfolio = models.BooleanField(
        null=False, blank=False, default=False,
        help_text='Este trabalho deve entrar no portfolio da EITA?')
    encerrado = models.BooleanField(
        null=False, blank=False, default=False,
        help_text='O projeto foi encerrado e não existe mais')
    tecnologias = models.ManyToManyField(Tecnologia, related_name='projetos', blank=True)
    produtos = models.ManyToManyField(TipoProduto, related_name='projetos', blank=True)
    temas = models.ManyToManyField(Tema, related_name='projetos', blank=True)
    link = models.CharField(max_length=100, null=True, blank=True)
    repositorio = models.CharField(max_length=100, null=True, blank=True)
    imagem = models.ImageField(null=True, blank=True)
    contatos = models.ManyToManyField(Contato,
        related_name='projetos', blank=True)

    backup = models.BooleanField(
        null=False, blank=False, default=False,
        help_text='Este trabalho tem a cópia de segurança sob responsabilidade da EITA?')
    backup_nome_identificacao = models.CharField(
        'Nome de identificação do backup',
        max_length=100, null=True, blank=True)
    backup_bucketname = models.ForeignKey(
        BackupBucketName, on_delete=models.CASCADE,
        verbose_name='aws s3 bucketname',
        help_text='Ex.: s3://bucketname',
        null=True, blank=True,
        related_name='trabalhos')
    backup_aws_config = models.CharField(
        verbose_name='aws s3 config',
        max_length=100, null=True, blank=True,
        help_text='Informe o aws config do servidor de verificação de backup.<br>Deixe vazio para utilizar a configuração padrão.')

    infraestrutura_servidor = models.ForeignKey(
        InfraestruturaServidor, on_delete=models.CASCADE,
        verbose_name='Servidor (infraestrutura)',
        null=True, blank=True,
        related_name='trabalhos')

    class Meta:
        '''
        ordering = ['-orcamentos__mes_ultima_retirada',
                    '-orcamentos__horas_utilizadas_na_ultima_retirada',
                    'nome']
        '''
        verbose_name = 'Trabalho'
        ordering = ['nome',]

    def __str__(self):
        return f"{self.nome}"

    def atualizar_orcamento(self):
        ## TODO rever metodo de atualizar orçamento, considerando que agora
        ## se relaciona diretamente na retirada, facilitando a contabilização de horas
        return

        orcamento = self.get_orcamento_ativo()
        if not orcamento:
            return

        horas_utilizadas = 0
        # valor_utilizado = 0
        mes_ultima_retirada = ''
        horas_utilizadas_na_ultima_retirada = 0
        saldo_horas = 0

        retiradas = self.retiradas.all().order_by(
            '-retirada__data', '-data_insercao')

        if not retiradas:
            return

        # cooperada_valor_hora = settings.COOPERADA_VALOR_HORA
        for retirada in retiradas:
            if not retirada.horas:
                continue

            horas_utilizadas += retirada.horas
            cooperada_valor_hora = \
                    retirada.retirada.get_cooperada_valor_hora_retirada(recalcular=True)
            cooperada_valor_hora = Decimal(str(cooperada_valor_hora))
            # valor_retirada = retirada.horas * cooperada_valor_hora
            # valor_utilizado += valor_retirada

            # TODO Projeto > atualizar_orcamento: alterar para data referência
            data_retirada = retirada.retirada.data
            if not data_retirada:
                data_retirada = retirada.retirada.data_insercao
            mes_retirada = f"{data_retirada.year}-{data_retirada.month}"
            if not mes_ultima_retirada:
                mes_ultima_retirada = mes_retirada
            if mes_ultima_retirada == mes_retirada:
                horas_utilizadas_na_ultima_retirada += retirada.horas
        valor_utilizado = round(valor_utilizado, 2)

        orcamento.horas_utilizadas = horas_utilizadas
        orcamento.valor_utilizado = 0
        orcamento.mes_ultima_retirada = mes_ultima_retirada
        orcamento.horas_utilizadas_na_ultima_retirada = horas_utilizadas_na_ultima_retirada

        orcamento.save()

    def get_orcamento_ativo(self):

        try:
            return self._orcamento_ativo
        except:
            pass

        # TODO Verificar critério!
        orcamento = self.orcamentos.filter(
            aprovado_em__isnull=False
            # saldo_horas__gte=retirada_projeto.horas
            # ...
        ).first()
        if not orcamento:
            orcamento = self.orcamentos.create(aprovado_em='1500-01-01')

        self._orcamento_ativo = orcamento
        return orcamento

    # TODO descontinuar (passou direto para orçamento)
    def orcamento_valor(self):
        return self.get_orcamento_ativo().valor
    orcamento_valor.short_description = 'Valor aprovado'

    # TODO descontinuar (passou direto para orçamento)
    def valor_utilizado(self):
        return self.get_orcamento_ativo().valor_utilizado
    valor_utilizado.short_description = 'Valor utilizado'

    # TODO descontinuar (passou direto para orçamento)
    def orcamento_horas(self):
        return self.get_orcamento_ativo().horas
    orcamento_horas.short_description = 'Total de horas'

    # TODO descontinuar (passou direto para orçamento)
    def horas_utilizadas(self):
        return self.get_orcamento_ativo().horas_utilizadas
    horas_utilizadas.short_description = 'Horas utilizadas'

    # TODO descontinuar (passou direto para orçamento)
    def mes_ultima_retirada(self):
        return self.get_orcamento_ativo().mes_ultima_retirada
    mes_ultima_retirada.short_description = 'Ultima retirada'

    # TODO descontinuar (passou direto para orçamento)
    def horas_utilizadas_na_ultima_retirada(self):
        return self.get_orcamento_ativo().horas_utilizadas_na_ultima_retirada
    horas_utilizadas_na_ultima_retirada.short_description = 'Total de horas da ultima retirada'

    @property
    def ano_de_inicio(self):
        ''' Retorna o ano de início do trabalho a partir do ano do primeiro orçamento'''
        orcamentos = Orcamento.objects.filter(projeto=self).order_by('inicio')
        if orcamentos.count() == 0:
            return None
        else:
            primeiro_orcamento = orcamentos.first()
            if primeiro_orcamento.inicio:
                return primeiro_orcamento.inicio.year
            else:
                return None

    @property
    def beneficiarios_e_clientes(self):
        beneficiarios = []
        if self.beneficiario.count() > 0:
            beneficiarios = [b for b in self.beneficiario.all()]
        clientes = []
        for orcamento in self.orcamentos.all():
            if orcamento.cliente:
                clientes.append(orcamento.cliente)
        beneficiarios_e_clientes = list(set(clientes + beneficiarios))
        if beneficiarios_e_clientes:
            return ", ".join([cliente.nome for cliente in beneficiarios_e_clientes])
        else:
            return ""

    @property
    def ultimo_orcamento_aprovado(self):
        return self.get_orcamento_ativo().aprovado_em


class AtividadeCategoria(Base):
    nome = models.CharField(
        max_length=100, null=False, blank=False,
        unique=True)
    slug = models.SlugField(null=True, unique=True,
                            help_text='Slug é um termo jornalístico. \
                            Um slug é um rótulo curto para algo, contendo apenas letras, \
                            números, sublinhados ou hifens. Eles geralmente são usados em URLs.')

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome',]
        verbose_name = "Atividade - Categoria"
        verbose_name_plural = "Atividade - Categorias"


class Atividade(Base):
    '''
    - Anotar as horas relacionando com atividade realizada:
    . Programação
    . Design
    . Admin
    . Reunião Semanal
    . Infraestrtura
    . Formação
    . Direitos
    '''
    categoria = models.ForeignKey(
        AtividadeCategoria, on_delete=models.PROTECT,
        null=True, blank=True,
        related_name='atividades')
    nome = models.CharField(
        max_length=100, null=False, blank=False,
        unique=True)
    slug = models.SlugField(null=True, unique=True,
                            help_text='Slug é um termo jornalístico. \
                            Um slug é um rótulo curto para algo, contendo apenas letras, \
                            números, sublinhados ou hifens. Eles geralmente são usados em URLs.')

    def __str__(self):
        atividade = f"{self.nome}"
        if self.categoria:
            atividade += f" ({self.categoria})"
        return atividade


class Orcamento(Base):
    nome = models.CharField(
            'orçamento', max_length=100, null=True, blank=True)
    slug = models.SlugField(
            null=True, blank=True,
            help_text='Slug é um termo jornalístico. \
                    Um slug é um rótulo curto para algo, contendo apenas letras, \
                    números, sublinhados ou hifens. Eles geralmente são usados em URLs.')
    projeto = models.ForeignKey(
        Projeto, on_delete=models.CASCADE,
        verbose_name='trabalho',
        null=True, blank=False,
        related_name='orcamentos')
    cliente = models.ForeignKey(
        Cliente, on_delete=models.PROTECT,
        null=True, blank=True)
    valor = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    # TODO checar utilização do campo horas do orçamento pois esse controle passou para atividade
    horas = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    aprovado_em = models.DateField(null=True, blank=True)
    inicio = models.DateField(null=True, blank=True)
    encerramento_previsto = models.DateField(null=True, blank=True)
    encerramento = models.DateField(null=True, blank=True)

    valor_utilizado = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    horas_utilizadas = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    # TODO checar horas_utilizadas_na_ultima_retirada em Orcamento.
    horas_utilizadas_na_ultima_retirada = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    # mes_ultima_retirada = models.CharField(
    #     max_length=10, blank=False, null=False, default='1500-01')

    # saldo_horas é utilizado para mostra horas disponíveis quando da retirada.
    saldo_horas = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    observacoes = models.TextField(
            'observações',
            null=True, blank=True)

    class Meta:
        ordering = [# '-mes_ultima_retirada',
                'projeto__nome',
                '-aprovado_em',
                '-id',
                # 'aprovado_em',
                # 'id',
                'nome',
                # '-horas_utilizadas_na_ultima_retirada',
                # '-data_insercao'
                ]
        verbose_name = "Orçamento"

    def __str__(self):
        horas = f"{self.horas} horas".replace('.', ',') if self.horas else ''
        valor = f"R$ {self.valor}".replace('.', ',') if self.valor else ''
        return ' - '.join([value
            for value in [
                f"{self.projeto}",
                f"{self.nome}",
                horas,
                valor,
                f"aprovado em {self.aprovado_em}"
                ]
            if value])

    def atualizar_orcamento_horas_utilizadas(self):
        orcamento_horas_utilizadas = 0
        for atividade in self.atividades.all():
            horas_utilizadas = atividade.atualizar_horas_utilizadas()

            if horas_utilizadas:
                orcamento_horas_utilizadas += horas_utilizadas

        self.horas_utilizadas = orcamento_horas_utilizadas
        self.save()

        return orcamento_horas_utilizadas


class OrcamentoAtividade(Base):
    orcamento = models.ForeignKey(
            Orcamento, on_delete=models.CASCADE,
            verbose_name='orçamento',
            related_name='atividades')
    atividade = models.ForeignKey(
            Atividade, on_delete=models.CASCADE,
            related_name='orcamentos')
    qtd_horas = models.DecimalField(
            verbose_name='Quantidade horas',
            max_digits=10, decimal_places=2,
            blank=True, null=True)
    horas_utilizadas = models.DecimalField(
            verbose_name='horas utilizadas',
            max_digits=10, decimal_places=2,
            blank=True, null=True)
    mes_ultima_retirada = models.CharField(
            max_length=10, default='1500-01',
            blank=True, null=True)
    encerramento_previsto = models.DateField(
            null=True, blank=True)
    encerramento = models.DateField(
            null=True, blank=True)

    class Meta:
        verbose_name = 'Orçamento Atividade'
        ordering = ['orcamento__projeto__nome', 'orcamento__nome', 'atividade__nome']

    def __str__(self):
        saldo_horas = f"{self.saldo_horas}".replace('.', ',')
        str_atividade = f"{self.orcamento.projeto} - {self.orcamento.nome} - {self.atividade}"
        if saldo_horas == 'None':
            return str_atividade
        else:
            return str_atividade + f" ({saldo_horas} horas)"

    @property
    def saldo_horas(self):
        # TODO calculo do saldo_horas
        try:
            return self.qtd_horas - self.horas_utilizadas
        except TypeError as e:
            return self.qtd_horas

    def atualizar_horas_utilizadas(self):
        if self.retiradas.count() == 0:
            self.horas_utilizadas = 0
            self.save()
            return 0

        ultima_retirada =\
                self.retiradas.order_by(
                        '-retirada__data',
                        '-retirada__data_insersao')\
                                .first().retirada
        if ultima_retirada.data:
            self.mes_ultima_retirada =\
                    ultima_retirada.data.strftime('%Y-%m')
        else:
            self.mes_ultima_retirada =\
                    ultima_retirada.data_insercao.strftime('%Y-%m')

        self.horas_utilizadas = \
                self.retiradas.aggregate(
                        Sum('horas'))['horas__sum']
        self.save()

        return self.horas_utilizadas

    def atualizar_horas_utilizadas_orcamento(self):
        self.orcamento.atualizar_orcamento_horas_utilizadas()


# class OrcamentoDocumento(Base):
    # orcamento = models.ForeignKey(
        # Orcamento, on_delete=models.CASCADE,
        # verbose_name='orçamento',
        # related_name='atividades')
    # atividade = models.ForeignKey(
        # Atividade, on_delete=models.CASCADE,
        # related_name='orcamentos')
    # qtd_horas = models.DecimalField(
        # verbose_name='Quantidade horas',
        # max_digits=10, decimal_places=2,
        # blank=True, null=True)

    # class Meta:
        # verbose_name = 'Orçamento Atividade'
        # ordering = ['orcamento__projeto__nome', 'atividade__nome']

    # def __str__(self):
        # saldo_horas = f"{self.saldo_horas}".replace('.', ',')
        # return f"{self.orcamento.projeto} - {self.atividade} ({saldo_horas} horas)"


class Reuniao(models.Model):
    titulo = models.CharField('título', max_length=100, null=False, blank=False)
    data = models.DateField(null=False, blank=False)
    orcamento = models.ForeignKey(
        Orcamento, on_delete=models.CASCADE,
        verbose_name='orçamento',
        null=True, blank=True,
        related_name='reunioes')
    anotacoes = models.TextField(
            'anotações',
            null=False, blank=False)
    participantes = models.ManyToManyField(Cooperada, related_name='reunioes', blank=True)

    def __str__(self):
        return f"{self.data} - {self.orcamento} - {self.titulo}"

    class Meta:
        ordering = ['-data', 'orcamento__projeto__nome', 'orcamento__nome', 'titulo']
        verbose_name = "Reunião"
        verbose_name_plural = "Reuniões"


class Categoria(Base):
    nome = models.CharField(
        max_length=100, null=True, blank=False)

    def __str__(self):
        return f"{self.nome}"

    class Meta:
        ordering = ['nome']


class CategoriaPagamento(Categoria):
    pass


class CategoriaEntrada(Categoria):
    pass


class Tributo(Base):
    '''
    ir: 1,5
    csll: 1
    pis: 0,65
    cofins: 3
    '''
    nome = models.CharField(max_length=100, unique=True)
    sigla = models.CharField(max_length=100, unique=True)
    descricao = models.CharField('descrição', max_length=400, null=True, blank=True)
    aliquota = models.DecimalField('alíquota', max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.sigla} ({self.aliquota}%)"

    class Meta:
        verbose_name = "tributo faturamento"
        verbose_name_plural = "tributos faturamento"


class Movimento(Base):
    valor = models.DecimalField('valor líquido',
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    valor_bruto = models.DecimalField('valor bruto',
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    valor_tributo = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True,
        help_text='Para os casos do valor do tributo ser declarado diretamente e não selecionados separadamente.')
    data = models.DateField('data quitação', null=True, blank=True)
    descricao = models.CharField(
        'descrição', max_length=400,
        null=True, blank=True,
        # help_text='Esta será a descrição da movimentação'
    )
    conta = models.ForeignKey(
        ContaBancaria, on_delete=models.CASCADE,
        null=True, blank=True,
        related_name='movimentos')
    conta_saldo = models.DecimalField('saldo',
        max_digits=10, decimal_places=2,
        blank=True, null=True)

    tributos = models.ManyToManyField(Tributo, through='TributoMovimento', related_name='movimentos')
    outra_origem = models.CharField(
        max_length=100, null=True, blank=True,
        help_text='No caso de pagamentos, será atribuída "EITA" \
        caso nenhuma origem seja informada'
    )
    posicao_lancamento = models.PositiveBigIntegerField(
        null=True, blank=True)

    objects = InheritanceManager()

    class Meta:
        '''
        ordering = ['data', 'id']
        ordering = ['-data', '-id']
        '''
        ordering = ['-data', '-posicao_lancamento']
        verbose_name = 'movimento financeiro'
        verbose_name_plural = 'movimentação financeira'

    def __init__(self, *args, **kwargs):
        super(Movimento, self).__init__(*args, **kwargs)
        self._conta_presave = self.conta
        # super().__init__(*args, **kwargs)

    def save(self, atualizar_saldo=True, *args, **kwargs):
        if not self.posicao_lancamento:
            self.posicao_lancamento = self.gerar_posicao_lancamento()

        if not self.conta:
            self.conta_saldo=None

        result = super(Movimento, self).save(*args, **kwargs)

        if not atualizar_saldo:
            return result

        if self.conta and self.valor!=None:
            self.conta.data_ultimo_lancamento = date.today()
            self.conta.save()
            self.conta.consolidar_saldo()
            # if self.data:
            #     self.conta.atualizar_saldo_lancamentos()
            self.conta.atualizar_saldo_lancamentos()

        if (self._conta_presave and
                self.conta != self._conta_presave and
                self.valor!=None):
            self._conta_presave.atualizar_saldo_lancamentos()
            self.conta.consolidar_saldo()

        return result

    def delete(self, *args, **kwargs):
        super(Movimento, self).delete(*args, **kwargs)

        if self._conta_presave:
            self._conta_presave.atualizar_saldo_lancamentos()

    @property
    def origem(self):
        try:
            self.saida
            return 'EITA'
        except:
            pass

        try:
            return self.entrada.orcamento
        except:
            pass

        return ''

    @property
    def beneficiario(self):
        try:
            return self.saida.retirada.cooperada
        except:
            pass

        try:
            return self.saida.pagamento.destino
        except:
            pass

        try:
            self.entrada
            return 'EITA'
        except:
            pass

        return ''

    @property
    def _categoria(self):
        try:
            self.saida.retirada
            return 'Retirada'
        except:
            pass

        try:
            return self.saida.pagamento.categoria
        except:
            pass

        try:
            return self.entrada.categoria
        except:
            pass

        return ''

    def gerar_posicao_lancamento(self):
        return datetime.today().strftime('%y%m%d%H%M%S%f')

    def atualiza_tributos(self, list_tributos_id):
        self.tributos_retidos.all().delete()

        tributos_retidos = Tributo.objects.filter(
            id__in=list_tributos_id)
        for tributo in tributos_retidos:
            TributoMovimento.objects.create(
                tributo = tributo,
                movimento_id = self.id,
                aliquota_retida = tributo.aliquota
            )


class TributoMovimento(Base):
    tributo = models.ForeignKey(
        Tributo, on_delete=models.CASCADE)
    movimento = models.ForeignKey(
        Movimento, on_delete=models.CASCADE,
        related_name='tributos_retidos')
    aliquota_retida = models.DecimalField('alíquota', max_digits=10, decimal_places=2)

    class Meta:
        unique_together = ['tributo', 'movimento']


class Saida(Movimento):
    quitado_em = models.DateField(null=True, blank=True)

    def __str__(self):
        return f"{self.descricao}"

    class Meta:
        verbose_name = 'movimento - saída'
        verbose_name_plural = 'movimento - saídas'

    def save(self, *args, **kwargs):
        super(Saida, self).save(*args, **kwargs)

        if self.valor!=None:
            self.valor = float(self.valor)
            if self.valor > 0:
                self.valor = self.valor*(-1)

        if self.valor_bruto!=None:
            self.valor_bruto = float(self.valor_bruto)
            if self.valor_bruto > 0:
                self.valor_bruto = self.valor_bruto*(-1)

        if not self.quitado_em and self.data:
            self.quitado_em=self.data

        super(Saida, self).save(*args, **kwargs)


class Pagamento(Saida):
    # TODO alterar campo abaixo para Cliente
    projeto = models.ForeignKey(
        Projeto, on_delete=models.CASCADE,
        verbose_name='trabalho',
        null=True, blank=True)
    destino = models.CharField(
        max_length=100, null=True, blank=True,
        help_text='Beneficiário do pagamento')
    categoria = models.ForeignKey(
        CategoriaPagamento, on_delete=models.CASCADE,
        null=True, blank=True, related_name='movimento')

    def __str__(self):
        return f"{self.data_insercao} - {self.projeto} | {self.descricao}"

    class Meta:
        ordering = ['-id']


from .exceptions import (validar_retirada_projetos,
                         validar_horas_trabalhadas_textual)


class Retirada(Saida):
    data_referencia = models.DateField(
        'Mês de referência',
        null=True, blank=True)
    cooperada = models.ForeignKey(
        Cooperada, on_delete=models.CASCADE,
        related_name='retiradas',
        verbose_name='Trabalhador(a)',
        null=True, blank=False) # TODO definir related_name
    horas_trabalhadas = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)

    horas_trabalhadas_diff = models.DecimalField(
        'Horas trabalhadas com diferença',
        max_digits=10, decimal_places=2,
        blank=True, null=True)

    # TODO horas_iteraveis campo deprecated, retirar!
    horas_iteraveis = models.TextField(
        null=True, blank=True,
        validators=[validar_retirada_projetos],
        help_text='ex.: Reunião Semanal: 4,5; Descanso: 20; Bambirra: 15,4')

    # TODO Validar cooperada_valor_hora nulo
    cooperada_valor_hora = models.DecimalField(
        # default=settings.COOPERADA_VALOR_HORA,
        max_digits=10, decimal_places=2,
        blank=True, null=True)

    def get_anexo_atestado_dir(instance, filename):
        data_retirada = instance.data
        if not data_retirada:
            data_retirada=date.today()
        data_retirada = data_retirada.strftime('%Y-%m-%d')
        return 'atestados/{0}/{1}/{2}'.format(
            instance.cooperada.user.username,
            data_retirada,
            filename
        )
    anexo_atestado = models.FileField(
        'anexo do atestado',
        upload_to=get_anexo_atestado_dir,
        blank=True, null=True)

    dias_ferias = models.PositiveSmallIntegerField(
        'Qtde. dias de férias',
        null=True, blank=True,
        help_text='Informe o número de dias.'
    )
    valor_ferias = models.DecimalField(
        'valor férias',
        max_digits=10, decimal_places=2,
        blank=True, null=True)

    valor_trabalho = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=True, null=True)

    def __str__(self):
        data = self.retirada.data
        data = data.strftime('%d/%m/%Y') if data else ''
        return f"{data} - {self.cooperada} | {self.descricao}"

    # class Meta:
    #     ordering = ['-id']

    _original_dias_ferias = None
    _original_dias_ferias_check_save = None

    def __init__(self, *args, **kwargs):
        super(Retirada, self).__init__(*args, **kwargs)
        self._original_dias_ferias = self.dias_ferias
        self._original_dias_ferias_check_save = self.dias_ferias
        self._registro_ferias_original = {}
        for registro_ferias in self.registro_ferias.order_by('cooperada_ferias_saldo__ano'):
            ano_obj = registro_ferias.cooperada_ferias_saldo
            saldo = registro_ferias.saldo_anterior
            self._registro_ferias_original[ano_obj] = saldo

    def save(self, *args, **kwargs):
        super(Retirada, self).save(*args, **kwargs)
        self.add_retirada_projetos()
        self.cooperada.update_ano_ultima_atividade()
        self.save_retirada_ferias()
        # super().save(*args, **kwargs)

    def save_retirada_ferias(self):

        # Verifica se tem alteração no campo dias_ferias
        if (self.dias_ferias == self._original_dias_ferias_check_save
                or (self.dias_ferias == '' and self._original_dias_ferias_check_save == None)):
            return None

        self._original_dias_ferias_check_save = self.dias_ferias

        self.update_ferias_extrato()

        self.calcular_valor_ferias()

        self.calcular_retirada()

    def update_ferias_extrato(self, on_delete=False):
        instance_retirada = self

        cooperada = instance_retirada.cooperada

        data_retirada = instance_retirada.data_referencia
        if not data_retirada:
            data_retirada=date.today()

        ## Deletar todas RetiradaFerias de cada retirada;
        RetiradaFerias.objects.filter(
            Q(cooperada_ferias_saldo__cooperada=cooperada) &
            Q(Q(retirada__data_referencia__gt=data_retirada) |
              Q(retirada__data_referencia__isnull=True))
        ).delete()

        ## Calcular saldo_anterior das RetiradasFerias da instância,
        ## que será o disparo para as próximas contabilizações;
        if on_delete:
            for saldo_ferias in self._registro_ferias_original:
                saldo_ferias.saldo = self._registro_ferias_original[saldo_ferias]
                saldo_ferias.save()

            saldo_ferias_ano_updated = saldo_ferias.ano
        else:
            instance_retirada_registro_ferias_obj = \
                instance_retirada.registro_ferias
            if instance_retirada_registro_ferias_obj.count() > 0:
                instance_retirada_registro_ferias = \
                    instance_retirada_registro_ferias_obj.first()
            else:
                ferias_saldo = self.cooperada.ferias_saldo\
                    .filter(
                        ano__lte=data_retirada.year)\
                    .exclude(
                        Q(saldo__isnull=True) | Q(saldo=0))
                ano_saldo = ferias_saldo.first()
                instance_retirada_registro_ferias = \
                    instance_retirada.registro_ferias.create(
                        cooperada_ferias_saldo = ano_saldo,
                        valor_hora = ano_saldo.get_valor_hora(),
                        saldo_anterior = ano_saldo.saldo
                    )

            instance_retirada_saldo_anterior = \
                instance_retirada_registro_ferias.saldo_anterior
            cooperada_ferias_saldo = instance_retirada_registro_ferias.cooperada_ferias_saldo
            saldo_ferias_ano_updated = cooperada_ferias_saldo.ano
            saldo_anterior = instance_retirada_saldo_anterior - instance_retirada.dias_ferias

            if saldo_anterior >= 0:
                cooperada_ferias_saldo.saldo = saldo_anterior
                instance_retirada_registro_ferias.dias_ferias = instance_retirada.dias_ferias
            else:
                """
                O saldo do ano, ordenado crescente, não foi suficiente para qtde de dias de férias. Portanto:
                Verifica se o ano seguinte a este primeiro registro seja menor ou igual a data de retirada. Então:
                * registre o lançamento em RetiradaFerias.
                Caso seja maior, significa que foi determinado um número de dias de férias maior que o saldo, retorna exceção.
                """

                cooperada_ferias_saldo.saldo = 0

                instance_retirada_registro_ferias.dias_ferias = instance_retirada_saldo_anterior

                dias_ferias_ano_seguinte = instance_retirada.dias_ferias - instance_retirada_saldo_anterior

                ano_saldo_ferias = instance_retirada_registro_ferias.cooperada_ferias_saldo.ano
                ano_seguinte_saldo_ferias = ano_saldo_ferias+1

                if ano_seguinte_saldo_ferias <= data_retirada.year:

                    saldo_anterior = saldo_anterior*(-1)

                    cooperada_ferias_saldo_ano_seguinte=instance_retirada.cooperada.ferias_saldo.get(
                        ano=ano_seguinte_saldo_ferias
                    )

                    saldo_ferias_ano_updated = cooperada_ferias_saldo_ano_seguinte.ano

                    registro_ferias_ano_seguinte, created = \
                        instance_retirada.registro_ferias.get_or_create(
                            cooperada_ferias_saldo=cooperada_ferias_saldo_ano_seguinte,
                        )
                    registro_ferias_ano_seguinte.dias_ferias = dias_ferias_ano_seguinte
                    registro_ferias_ano_seguinte.saldo_anterior = 30
                    registro_ferias_ano_seguinte.valor_hora=cooperada_ferias_saldo.get_valor_hora()
                    registro_ferias_ano_seguinte.save()

                    cooperada_ferias_saldo_ano_seguinte.saldo = \
                        registro_ferias_ano_seguinte.saldo_anterior - saldo_anterior
                    cooperada_ferias_saldo_ano_seguinte.save()
                else:
                    raise Exception(
                        "Erro ao recalcular o saldo de férias: Não há saldo de férias para esta contabilização."
                    )

            instance_retirada_registro_ferias.save()
            cooperada_ferias_saldo.save()

        # atualiza saldo ferias da cooperada,
        # com ano maior do que o ultimo atualizado acima.
        for saldo_ferias in CooperadaFeriasSaldo.objects.filter(
                cooperada=instance_retirada.cooperada,
                ano__gt=saldo_ferias_ano_updated):
            saldo_ferias.saldo=30
            saldo_ferias.save()

        ## Listar retiradas com data maior que da instância;
        retiradas_seguintes = Retirada.objects.filter(
            cooperada=instance_retirada.cooperada,
            data_referencia__gt=data_retirada, dias_ferias__gt=0
        ).order_by('data_referencia')
        for retirada in retiradas_seguintes:
            retirada.recompor_lancamentos_ferias()
        # Considerar retiradas sem data
        # Não considerar se uma nova retirar for inserida
        retiradas_seguintes_sem_data = Retirada.objects\
            .filter(
                cooperada=instance_retirada.cooperada,
                data_referencia__isnull=True, dias_ferias__gt=0
            )\
            .exclude(
                data_insercao=self.data_insercao,
            )\
            .order_by('data_insercao')
        for retirada in retiradas_seguintes_sem_data:
            retirada.recompor_lancamentos_ferias()

    def recompor_lancamentos_ferias(self):
        """
        * Calcular saldo anterior do lançamento a ser criado;
        * Criar lançamento de férias de retirada,
          com base no ano crescente
        """

        data_retirada = self.data_referencia
        if not data_retirada:
            data_retirada=date.today()

        qtde_dias_por_ano = self.dias_ferias
        valor_ferias = 0

        # Listar férias pendentes da cooperada
        ferias_saldo = self.cooperada.ferias_saldo\
            .filter(
                ano__lte=data_retirada.year)\
            .exclude(
                Q(saldo__isnull=True) | Q(saldo=0))
        for ano_saldo in ferias_saldo:

            """
            Atualizar EXTRATO de férias em RetiradaFerias
            """
            ano_saldo_retirada, created = ano_saldo.retiradas\
                .get_or_create(
                    retirada = self
                )
            # caso seja alteração, atualizar
            # dias_ferias com saldo_anterior
            if created:
                # TODO conferir o por que da linha seguinte!!!
                ano_saldo_retirada.dias_ferias = ano_saldo.saldo
                ano_saldo_retirada.saldo_anterior = ano_saldo.saldo
            else:
                ano_saldo_retirada.dias_ferias = ano_saldo_retirada.saldo_anterior
                ano_saldo.saldo = ano_saldo_retirada.saldo_anterior

            if qtde_dias_por_ano < ano_saldo_retirada.dias_ferias:
                ano_saldo_retirada.dias_ferias = qtde_dias_por_ano

            valor_hora = ano_saldo.get_valor_hora()
            ano_saldo_retirada.valor_hora = valor_hora
            ano_saldo_retirada.save()

            qtde_dias_por_ano = qtde_dias_por_ano - ano_saldo_retirada.dias_ferias

            """
            Atualizar SALDO de férias em CooperadaFeriasSaldo
            """
            ano_saldo.saldo = ano_saldo.saldo - ano_saldo_retirada.dias_ferias
            ano_saldo.save()

    def calcular_valor_ferias(self):
        registro_ferias_qs = self.registro_ferias.all()
        valor_ferias = 0
        for retirada_ferias in registro_ferias_qs:
            """
            Calcular VALOR das férias
            """
            valor_hora = retirada_ferias.cooperada_ferias_saldo.get_valor_hora()
            media_horas_ferias = self.cooperada.calc_media_horas_ferias(self.data_referencia)
            horas_ferias = media_horas_ferias * retirada_ferias.dias_ferias
            valor_ferias_ano = horas_ferias * valor_hora
            valor_ferias = valor_ferias + valor_ferias_ano
        self.valor_ferias = valor_ferias
        self.save()

    # TODO Metodo add_retirada_projetos deprecated!
    def add_retirada_projetos(self):
        if self.horas_iteraveis:
            self._calcular_retirada_textual()

    def calcular_retirada(self):

        # TODO: refatorar este metodo / chamada!
        # idealmente este metodo deve ser chamado por este mesmo objeto.
        # Porém entra em recursão pois não atualiza a FK `self.projetos` a tempo.
        # Ver: http://www.petrounias.org/articles/2010/04/05/propagating-field-changes-in-django-model-instance-aliases/

        # TODO refatorar chamada retirada.horas_iteraveis no metodo calcular_retirada
        if self.retirada.horas_iteraveis:
            return

        self.horas_trabalhadas = 0
        for projeto in self.projetos.all():
            if projeto.horas:
                self.horas_trabalhadas += projeto.horas

        self.valor_trabalho = self.calcular_valor_retirada()

        valor = self.valor_trabalho
        if self.valor_ferias:
            valor = valor + float(self.valor_ferias)
        self.valor = valor
        self.valor_bruto = valor
        self.save()

    def get_projeto_by_name(self, nome_projeto):
        projeto = Projeto.objects.filter(nome__iexact=nome_projeto)
        if len(projeto) == 1:
            return projeto[0]

        if len(projeto) == 2:
            if projeto[0].id == projeto[1].id:
                return projeto[0]

        if len(projeto) > 1:
            projeto = Projeto.objects.filter(nome__exact=nome_projeto)
            if len(projeto) == 1:
                return projeto[0]

            elif len(projeto) == 2:
                if projeto[0].id == projeto[1].id:
                    return projeto[0]
                else:
                    raise Exception(f"Não foi possível concluir. Verifique duplicação no projeto {projeto[0]}")
            else:
                raise Exception(f"Não foi possível concluir. Verifique duplicação no projeto {nome_projeto}")

    def _calcular_retirada_textual(self):
        projetos_deletados = [retirada.projeto
                              for retirada in self.projetos.all()]
        self.projetos.all().delete()
        retirada_horas_total = 0
        projetos = self.horas_iteraveis.split(';')
        for projeto in projetos:
            if not projeto:
                continue

            retirada_projeto = RetiradaProjeto()
            retirada_projeto.retirada = self

            nome_projeto, horas = projeto.split(':')
            projeto = self.get_projeto_by_name(nome_projeto.strip())
            retirada_projeto.projeto = projeto
            retirada_projeto.horas = self.normalizar_horas(horas)
            retirada_projeto.horas_trabalhadas_textual = horas.strip()
            try:
                retirada_projeto.save()

                try:
                    projetos_deletados.remove(projeto)
                except:
                    pass

            except IntegrityError:
                raise ValidationError('Lista de projetos inválida. Verifique duplicação em nomes de projetos',
                                      code='retirada_projetos_error')

            retirada_horas_total += retirada_projeto.horas

        self.horas_trabalhadas = retirada_horas_total
        valor = self.calcular_valor_retirada()
        self.valor = valor
        self.valor_bruto = valor
        self.horas_iteraveis = ''
        self.save()

        self._projetos_atualizar_orcamentos(projetos_deletados)

    def _projetos_atualizar_orcamentos(self, projetos_list):
        projetos_list = set(projetos_list)
        for projeto in projetos_list:
            projeto.atualizar_orcamento()

    def normalizar_horas(self, horas):
        return float(horas.strip().replace(',', '.'))

    def calcular_valor_retirada(self):
        cooperada_valor_hora = self.get_cooperada_valor_hora_retirada()
        return float(self.horas_trabalhadas) * cooperada_valor_hora

    def convert_to_date(self, param_date):
        param_date = param_date.split('-')
        return date(int(param_date[0]),
                int(param_date[1]),
                int(param_date[2]))

    def get_data_ref_to_compare(self):
        data_ref = self.data_referencia
        if type(data_ref) == str:
            data_ref = self.convert_to_date(data_ref)

        return data_ref

    def compare_data_valor_hora_historico(self, param_date):
        data_vigencia_valor_hora = self.convert_to_date(param_date)
        data_ref_to_compare = self.get_data_ref_to_compare()

        return data_ref_to_compare >= data_vigencia_valor_hora

    def get_cooperada_valor_hora_retirada(self, recalcular=False):
        if not self.cooperada_valor_hora or recalcular:
            cooperada_valor_hora_dict = settings.COOPERADA_VALOR_HORA_HISTORICO
            valor_hora = {
                k: v
                for k, v in cooperada_valor_hora_dict.items()
                if self.compare_data_valor_hora_historico(k)
            }
            try:
                valor_hora = list(valor_hora.values())[-1]
            except IndexError:
                lista = list(cooperada_valor_hora_dict)
                lista.sort()
                vigencia_valor_hora_inicial = self.convert_to_date(lista[0])
                if self.data_referencia < vigencia_valor_hora_inicial:
                    valor_hora = cooperada_valor_hora_dict[lista[0]]

            self.cooperada_valor_hora = valor_hora
            self.save()

        if self.cooperada_valor_hora == None:
            return settings.COOPERADA_VALOR_HORA

        return float(self.cooperada_valor_hora)

@receiver(models.signals.post_delete, sender=Retirada)
def auto_delete_retirada_anexo_atestado_on_delete(sender, instance, **kwargs):
    if instance.anexo_atestado:
        if os.path.isfile(instance.anexo_atestado.path):
            os.remove(instance.anexo_atestado.path)

@receiver(models.signals.pre_save, sender=Retirada)
def auto_delete_retirada_anexo_atestado_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_file = Retirada.objects.get(pk=instance.pk).anexo_atestado
    except Retirada.DoesNotExist:
        return False

    new_file = instance.anexo_atestado
    if not old_file == new_file:
        if old_file and os.path.isfile(old_file.path):
            os.remove(old_file.path)

@receiver(models.signals.post_delete, sender=Retirada)
def auto_recalcular_retirada_ferias_on_delete(sender, instance, **kwargs):
    if not instance.dias_ferias:
        return

    instance.update_ferias_extrato(on_delete=True)

def normalizar_horas_textual(horas):
    # recebe: 9999:99:99 | 9999:99 | 9999h99 | 9999h | 9999,99 | 9999.99
    # retorna: '9999:99:99'

    horas = re.sub(':$', '', horas.replace('h', ':'))
    horas = re.sub(r'(\d+:\d+)$', r'\1:00', horas)

    return horas

def converter_horas_textual(horas):
    horas = horas.replace(',', '.')
    try:
        return float(horas)
    except:
        horas = normalizar_horas_textual(horas)
        formula = re.sub(r"^(\d{1,5}):(\d{1,2}):(\d{1,2})$",
                         r"float('\1')+(float('\2')*(1/60)+(float('\3')*(1/3600)))",
                         horas)
        horas_to_decimal = eval(formula)
        horas_to_decimal = round(Decimal(horas_to_decimal), 2)

        return horas_to_decimal

class RetiradaProjeto(Base):
    retirada = models.ForeignKey(Retirada, on_delete=models.CASCADE,
                                 related_name='projetos')

    # TODO descontinuar campo projeto
    # Foi substituido por orcamento_atividade
    projeto = models.ForeignKey(Projeto, on_delete=models.CASCADE,
                                verbose_name='trabalho', blank=True,
                                related_name='retiradas')
    orcamento_atividade = models.ForeignKey(OrcamentoAtividade,
                                            on_delete=models.CASCADE,
                                            blank=False, null=True,
                                            related_name='retiradas')
    # verbosa
    horas_trabalhadas_textual = models.CharField(
        'horas trabalhadas', max_length=10, blank=False, null=True,
        help_text= 'Utilize o formato convencional de horas ou decimais',
        validators=[validar_horas_trabalhadas_textual],
    )
    # calculaveis
    horas = models.DecimalField(
        'Horas usadas para calculo',
        max_digits=10, decimal_places=2,
        blank=False, null=True)

    def __init__(self, *args, **kwargs):
        super(RetiradaProjeto, self).__init__(*args, **kwargs)
        self._horas_textual_presave = self.horas_trabalhadas_textual

        self._orcamento_atividade = self.orcamento_atividade

        try:
            self._projeto = self.projeto
        except:
            self._projeto = ''

        super(RetiradaProjeto, self).__init__(*args, **kwargs)

    def __str__(self):
        return f"{self.orcamento_atividade}"

    class Meta:
        ordering = ['-retirada__data']
        unique_together = ['retirada', 'projeto', 'orcamento_atividade']
        verbose_name = "registro de horas em atividade de projeto"
        verbose_name_plural = "registro de horas em atividades de projetos"

    def save(self, *args, **kwargs):
        super(RetiradaProjeto, self).save(*args, **kwargs)

        self._set_horas_decimal()

        ## TODO rever metodo de atualizar orçamento, considerando que agora
        ## se relaciona diretamente na retirada, facilitando a contabilização de horas
        # self._projeto_atualizar_orcamento()

        self.orcamento_atividade.atualizar_horas_utilizadas_orcamento()
        if (self._orcamento_atividade
                and self.orcamento_atividade != self._orcamento_atividade):
            self._orcamento_atividade.atualizar_horas_utilizadas_orcamento()

        # TODO: refatorar chamada!
        self.retirada.calcular_retirada()

        super(RetiradaProjeto, self).save(*args, **kwargs)

    def _projeto_atualizar_orcamento(self):
        if (self._horas_textual_presave != self.horas_trabalhadas_textual or
                self._projeto != self.projeto or
                not self.id):
            self.projeto.atualizar_orcamento()

    def _set_horas_decimal(self):
        if self._horas_textual_presave != self.horas_trabalhadas_textual:
            self.horas = self._converter_horas_textual()
            self._horas_textual_presave = self.horas_trabalhadas_textual
            try:
                self.save()
            except:
                raise
                # raise ValidationError(
                #     f"Não foi possível converter a hora {self.horas_trabalhadas_textual} do projeto {self.projeto}")

    def _converter_horas_textual(self):
        horas = self.horas_trabalhadas_textual
        return converter_horas_textual(horas)

    def delete(self, *args, **kwargs):
        super(RetiradaProjeto, self).delete(*args, **kwargs)
        # TODO: refatorar chamada!
        self.retirada.calcular_retirada()
        self._projeto_atualizar_orcamento()
        self._orcamento_atividade.atualizar_horas_utilizadas_orcamento()

    def show_horas_trabalhadas_textual(self):
        if not self.horas_trabalhadas_textual:
            return self.horas
        return self.horas_trabalhadas_textual
    show_horas_trabalhadas_textual.short_description = 'Horas trabalhadas'


class RetiradaFerias(Base):
    retirada = models.ForeignKey(
        Retirada, on_delete=models.CASCADE,
        related_name='registro_ferias')
    cooperada_ferias_saldo = models.ForeignKey(
        CooperadaFeriasSaldo, on_delete=models.CASCADE,
        related_name='retiradas')
    dias_ferias = models.PositiveSmallIntegerField(
        null=True, blank=True)
    saldo_anterior = models.PositiveSmallIntegerField(
        null=True, blank=True)
    valor_hora = models.DecimalField(
        null=True, blank=True,
        max_digits=10, decimal_places=2)

    class Meta:
        unique_together = ['retirada', 'cooperada_ferias_saldo']
        verbose_name = "registro de férias"
        verbose_name_plural = "registros de férias"

    def __str__(self):
        saldo_atual = self.saldo_anterior-self.dias_ferias
        return f"{self.cooperada_ferias_saldo.ano} - {self.dias_ferias} dias - Saldo de {saldo_atual} dias"


class Entrada(Movimento):
    categoria = models.ForeignKey(
        CategoriaEntrada, on_delete=models.CASCADE,
        null=True, blank=True, related_name='movimento')

    projeto = models.ForeignKey(
        Projeto, on_delete=models.CASCADE,
        verbose_name='trabalho',
        null=True, blank=True)

    orcamento = models.ForeignKey(
        Orcamento, on_delete=models.CASCADE,
        verbose_name='orçamento',
        related_name='lancamentos',
        null=True, blank=True)

    class Meta:
        ordering = ['-id']
        verbose_name = 'movimento - entrada'
        verbose_name_plural = 'movimento - entradas'
