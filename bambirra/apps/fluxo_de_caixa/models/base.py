from django.utils import timezone

from django.db import models
from django.contrib.auth.models import User


class Base(models.Model):
    # TODO retirar e usar LogEntry. Está mantido pois é utilizado na admin.
    data_insercao = models.DateTimeField(null=True, blank=True,
                                         verbose_name="Data de inserção",
                                         default=timezone.now)

    # TODO retirar e usar LogEntry. Está mantido pois é utilizado na admin.
    data_modificacao = models.DateTimeField(null=True, blank=True, verbose_name="Data da última modificação", auto_now=True)

    class Meta:
        abstract = True

    '''
    Para obter user/datas inserção/modificação basta acessar o LogEntry. Ex.:
    from django.contrib.admin.models import LogEntry, ADDITION
    LogEntry.objects.log_action(
        user_id         = request.user.pk,
        content_type_id = ContentType.objects.get_for_model(object).pk,
        object_id       = object.pk,
        object_repr     = force_unicode(object),
        action_flag     = ADDITION
    )

    O seguintes atributos não são mais necessários pois estão em LogEntry:

    usuario_insercao = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Criado por", related_name='insercao+')
    usuario_modificacao = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Modificado por", related_name='modificacao+')

    def save(self, *args, **kwargs):
        req = get_username()

        if req is not None:
            if not self.id:
                self.usuario_insercao = req.user

            self.usuario_modificacao = req.user

        return super().save(*args, **kwargs)
    '''
