import re
from django.core.exceptions import ValidationError
from django.utils.html import format_html
from .models import Projeto

def validar_retirada_projetos(value):
    projetos = [projeto[0]
                for projeto in Projeto.objects.values_list('nome')]

    """
    condições regex:
    * No início da string, inicia o agrupamento;
    * pode ter (ou não) muitos caracteres alfanuméricos;
    * Deve conter um nome idêntico de projeto da cooperativa;
    * Pode ou não ter alfanuméricos na sequência;
    * Dois pontos;
    * Numérico com ou sem decimal;
    * Pode ter ; na sequência;
    * Pode ou não ter alfanuméricos na sequência;
    * Finaliza o agrupamento, que se repete pelo menos uma vez.
    Ex.: Reunião Semanal: 2; Descanso: 20; Bambirra: 2
    """
    regex = rf"^(\s*({'|'.join(projetos)})\s*:\s*\d+([\.,]\d+)?;?\s*)+$"
    if not re.match(regex, value, re.IGNORECASE):
        raise ValidationError('Lista de projetos inválida. Verifique os nomes e formato.')

    # verifica duplicidade: regex para obter `Reunião Semanal,Descanso,Bambirra,`
    value_list = re.sub(r'\s*:\s*\d+([\.,]\d+)?;?\s*', ',', value).split(',')
    if len(value_list) != len(set(value_list)):
        raise ValidationError('Lista de projetos inválida. Verifique duplicidades.')

def validar_horas_trabalhadas_textual(value):
    padroes_horas = "9999:99:99 | 9999:99 | 9999h99 | 9999h | 9999,99 | 9999.99"

    regex = r"^\d{1,4}(:\d{1,2})?[:h,\.]?\d{0,2}$"

    if not re.match(regex, value, re.IGNORECASE):
        raise ValidationError(
            format_html(f"O formato das horas trabalhadas está incorreto. Verifique os padrões:<br>{padroes_horas}")
        )
