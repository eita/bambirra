# from django.contrib.admin.sites import AdminSite
from django.contrib.messages.storage.fallback import FallbackStorage
from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory, Client
from django.urls import reverse

from model_bakery import baker

from . import admin
from .models import models


class RetiradaBaseTestCase(TestCase):
    pass


class RetiradaTestCase(RetiradaBaseTestCase):

    def setUp(self):
        user_kwargs = {'username': 'user_name',
                       'password': 'password'}
        user = User.objects.create_superuser(**user_kwargs)
        cooperada = baker.make('Cooperada', user=user)
        self.client_cooperada = Client()
        self.client_cooperada.login(**user_kwargs)

        site = admin.BambirraAdminSite()
        self.admin = admin.RetiradaAdmin(
            models.Retirada, site)

        self.retiradaprojeto = baker.make('RetiradaProjeto')

        self.url_retirada = 'admin:fluxo_de_caixa_retirada'
        self.url_retiradaprojeto = 'admin:fluxo_de_caixa_retiradaprojeto'


    # Acessos

    def test_cooperada_acessar_lista_de_retirada(self): # [movimento]
        url = reverse(f'{self.url_retirada}_changelist')
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    def test_cooperada_acessar_nova_retirada(self): # [movimento]
        url = reverse(f'{self.url_retirada}_add')
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    def test_cooperada_acessar_edicao_de_retirada(self): # [movimento]
        url = reverse(f'{self.url_retirada}_change',
                      args=[self.retiradaprojeto.retirada.id])
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    def test_cooperada_acessar_exclusao_de_retirada(self): # [movimento]
        url = reverse(f'{self.url_retirada}_delete',
                      args=[self.retiradaprojeto.retirada.id])
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    def test_cooperada_acessar_lista_de_retiradaprojeto(self): # [movimento]
        url = reverse(f'{self.url_retiradaprojeto}_changelist')
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    def test_cooperada_acessar_nova_retiradaprojeto(self): # [movimento]
        url = reverse(f'{self.url_retiradaprojeto}_add')
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    def test_cooperada_acessar_edicao_de_retiradaprojeto(self): # [movimento]
        url = reverse(f'{self.url_retiradaprojeto}_change',
                      args=[self.retiradaprojeto.id])
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    def test_cooperada_acessar_exclusao_de_retiradaprojeto(self): # [movimento]
        url = reverse(f'{self.url_retiradaprojeto}_delete',
                      args=[self.retiradaprojeto.id])
        self.assertEqual(self.client_cooperada.get(url).status_code, 200)

    # Caminho feliz

    # test_efetuar_retirada
    # test_setar_horas_iteraveis_na_retirada
    # test_efetuar_retirada_sem_projetos

    # Validação

    # test_validar_horas_iteraveis_na_retirada
    # test_validar_formatos_de_horas_da_reirada
    # test_validar_pagamento_da_retirada[movimento]


# test_retirada_pagamento

# test_efetuar_pagamento_da_retirada
# test_efetuar_pagamento_de_retirada_sem_projetos
# test_inserir_retirada_e_pagamento_juntos
# test_calculo_das_horas_da_retirada
# test_calculo_do_valor_da_retirada
# test_impacto_na_conta_bancaria_quando_insere_retirada[movimento]
# test_impacto_na_conta_bancaria_quando_insere_retirada_e_pagamento_juntos
# test_impacto_na_conta_bancaria_quando_altera_conta_na_retirada[movimento]
# test_impacto_na_conta_bancaria_quando_altera_projetos_horas_na_retirada
# test_impacto_na_conta_bancaria_quando_altera_horas_iteraveis_na_retirada
# test_impacto_na_conta_bancaria_quando_deleta_retirada[movimento]


# test_retirada_orcamento

# test_impacto_da_retirada_nas_horas_do_orcamento_aprovado
# test_impacto_da_retirada_no_valor_do_orcamento_aprovado
# test_impacto_da_retirada_no_ranking_de_projetos

