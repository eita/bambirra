# Generated by Django 3.2.11 on 2022-02-03 04:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0080_cliente_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='orcamento',
            name='cliente',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='fluxo_de_caixa.cliente'),
        ),
    ]
