# Generated by Django 3.2.23 on 2024-01-31 19:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0133_auto_20240131_1612'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='retirada',
            name='horas_ferias',
        ),
    ]
