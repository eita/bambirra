# Generated by Django 3.1 on 2021-04-09 20:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0042_auto_20200903_0944'),
    ]

    operations = [
        migrations.CreateModel(
            name='Atividade',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_insercao', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Data de inserção')),
                ('data_modificacao', models.DateTimeField(auto_now=True, null=True, verbose_name='Data da última modificação')),
                ('nome', models.CharField(max_length=100, unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='projeto',
            options={'ordering': ['nome'], 'verbose_name': 'Trabalho'},
        ),
    ]
