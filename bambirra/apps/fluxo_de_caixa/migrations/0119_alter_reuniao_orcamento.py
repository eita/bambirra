# Generated by Django 3.2.20 on 2023-12-08 19:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0118_auto_20231208_1157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reuniao',
            name='orcamento',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='reunioes', to='fluxo_de_caixa.orcamento', verbose_name='orçamento'),
        ),
    ]
