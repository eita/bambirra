# Generated by Django 2.2.7 on 2019-12-19 14:27

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0010_auto_20191219_1023'),
    ]

    operations = [
        migrations.AddField(
            model_name='orcamento',
            name='aprovado_em',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='orcamento',
            name='data_registro',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
