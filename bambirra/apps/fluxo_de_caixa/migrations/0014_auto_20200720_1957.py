# Generated by Django 2.2.7 on 2020-07-20 22:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0013_auto_20191223_0933'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cooperada',
            options={'ordering': ['user__username']},
        ),
        migrations.AddField(
            model_name='retirada',
            name='horas_trabalhadas_diff',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True, verbose_name='Horas trabalhadas com diferença'),
        ),
    ]
