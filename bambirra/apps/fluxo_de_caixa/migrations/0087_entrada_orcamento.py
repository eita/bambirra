# Generated by Django 3.2.12 on 2022-02-22 14:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0086_alter_orcamentoatividade_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='entrada',
            name='orcamento',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='lancamentos', to='fluxo_de_caixa.orcamento', verbose_name='orçamento'),
        ),
    ]
