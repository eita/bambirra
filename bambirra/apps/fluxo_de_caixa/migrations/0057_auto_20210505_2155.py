# Generated by Django 3.1 on 2021-05-06 00:55

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0056_movimento_conta_saldo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tributo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_insercao', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Data de inserção')),
                ('data_modificacao', models.DateTimeField(auto_now=True, null=True, verbose_name='Data da última modificação')),
                ('nome', models.CharField(max_length=100, unique=True)),
                ('sigla', models.CharField(max_length=100, unique=True)),
                ('descricao', models.CharField(blank=True, max_length=400, null=True, verbose_name='descrição')),
                ('aliquota', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='alíquota')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='movimento',
            name='conta_saldo',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, verbose_name='saldo'),
        ),
        migrations.AlterField(
            model_name='pagamento',
            name='destino',
            field=models.CharField(blank=True, help_text='Beneficiário do pagamento', max_length=100, null=True),
        ),
        migrations.CreateModel(
            name='TributoMovimento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_insercao', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Data de inserção')),
                ('data_modificacao', models.DateTimeField(auto_now=True, null=True, verbose_name='Data da última modificação')),
                ('aliquota_retida', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='alíquota')),
                ('movimento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tributos', to='fluxo_de_caixa.movimento')),
                ('tributo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='movimentos', to='fluxo_de_caixa.tributo')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
