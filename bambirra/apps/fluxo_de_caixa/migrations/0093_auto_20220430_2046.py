# Generated by Django 3.2.13 on 2022-04-30 23:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0092_auto_20220430_1213'),
    ]

    operations = [
        migrations.AddField(
            model_name='orcamentoatividade',
            name='encerramento',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='orcamentoatividade',
            name='encerramento_previsto',
            field=models.DateField(blank=True, null=True),
        ),
    ]
