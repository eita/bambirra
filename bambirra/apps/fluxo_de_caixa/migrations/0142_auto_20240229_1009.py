# Generated by Django 3.2.23 on 2024-02-29 13:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0141_auto_20240229_1006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='entrada',
            options={'ordering': ['-id'], 'verbose_name': 'movimentação - entrada', 'verbose_name_plural': 'movimentação - entradas'},
        ),
        migrations.AlterModelOptions(
            name='movimento',
            options={'ordering': ['-data', '-posicao_lancamento'], 'verbose_name': 'movimentação financeira', 'verbose_name_plural': 'movimentações financeiras'},
        ),
        migrations.AlterModelOptions(
            name='saida',
            options={'verbose_name': 'movimentação - saída', 'verbose_name_plural': 'movimentação - saídas'},
        ),
    ]
