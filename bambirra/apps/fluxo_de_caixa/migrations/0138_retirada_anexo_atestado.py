# Generated by Django 3.2.23 on 2024-02-10 15:05

from django.db import migrations, models
import fluxo_de_caixa.models.models


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0137_alter_retirada_valor_ferias'),
    ]

    operations = [
        migrations.AddField(
            model_name='retirada',
            name='anexo_atestado',
            field=models.FileField(blank=True, null=True, upload_to=fluxo_de_caixa.models.models.Retirada.get_anexo_atestado_dir, verbose_name='anexo do atestado'),
        ),
    ]
