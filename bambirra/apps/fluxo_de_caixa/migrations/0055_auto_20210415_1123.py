# Generated by Django 3.1 on 2021-04-15 14:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0054_auto_20210415_1054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='atividade',
            name='slug',
            field=models.SlugField(help_text='Slug é um termo jornalístico.                             Um slug é um rótulo curto para algo, contendo apenas letras,                             números, sublinhados ou hifens. Eles geralmente são usados em URLs.', null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='projeto',
            name='slug',
            field=models.SlugField(help_text='Slug é um termo jornalístico.                             Um slug é um rótulo curto para algo, contendo apenas letras,                             números, sublinhados ou hifens. Eles geralmente são usados em URLs.', null=True, unique=True),
        ),
    ]
