# Generated by Django 3.2.20 on 2023-12-08 13:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0115_reuniao'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reuniao',
            name='anotacoes',
            field=models.TextField(default=django.utils.timezone.now, verbose_name='anotações'),
            preserve_default=False,
        ),
    ]
