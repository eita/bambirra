# Generated by Django 2.2.7 on 2020-08-04 13:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fluxo_de_caixa', '0022_retirada_cooperada_valor_hora'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='retiradaprojeto',
            unique_together={('retirada', 'projeto')},
        ),
    ]
