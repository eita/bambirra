from django.urls import include, path
from rest_framework_simplejwt import views as jwt_views

from fluxo_de_caixa import autocomplete, api
from . import views

urlpatterns = [
    # api fluxo de caixa
    path(
        'api/conta/<int:pk>/saldo/',
        api.conta_saldo,
        name='conta_saldo-api',
    ),
    path(
        'api/livro_caixa/',
        api.gerar_livro_caixa,
        name='movimento-gerar-livro_caixa',
    ),

    # FERIAS
    # path(
    #     'api/usuaria/<int:pk>/media_horas_ferias/',
    #     api.usuaria_media_horas_ferias,
    #     name='usuaria-media_horas_ferias',
    # ),
    path(
        'api/retirada/ferias_saldo/',
        api.retirada_ferias_saldo,
        name='retirada-ferias_saldo',
    ),

    # DESCANSO
    path(
        'api/retirada/horas_descanso/',
        api.retirada_horas_descanso,
        name='retirada-horas_descanso',
    ),
    path(
        'api/retirada/update_horas_descanso/',
        api.retirada_update_horas_descanso,
        name='retirada-update_horas_descanso',
    ),

    # ATESTADO
    path(
        'api/retirada/horas_atestado/',
        api.retirada_horas_atestado,
        name='retirada-horas_atestado',
    ),
    path(
        'api/retirada/update_horas_atestado/',
        api.retirada_update_horas_atestado,
        name='retirada-update_horas_atestado',
    ),

    # PROJETOS
    path(
        'api/projects/',
        api.ProtejosAPIView.as_view(),
        name='api-project-list',
    ),
]

urlpatterns += [
    # api backup
    path(
        'api/backup/',
        api.BackupView.as_view(),
        name='backup-api',
    ),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns += [
    # internal views
    path(
        'portfolio/',
        views.PortfolioListView.as_view(),
        name='portfolio-list',
    ),
]

urlpatterns += [
    # autocomplete urls
    path(
        'trabalho-atividade-autocomplete/',
        autocomplete.ProjetoAtividadeAutocomplete.as_view(),
        name='trabalho_atividade-autocomplete',
    ),
    path(
        'trabalho-atividade-ativo-autocomplete/',
        autocomplete.ProjetoAtividadeAtivoAutocomplete.as_view(),
        name='trabalho_atividade-ativo-autocomplete',
    ),
    path(
        'categoria-entrada-autocomplete/',
        autocomplete.CategoriaEntradaAutocomplete.as_view(create_field='nome'),
        name='categoria_entrada-autocomplete',
    ),
    path(
        'categoria-pagamento-autocomplete/',
        autocomplete.CategoriaPagamentoAutocomplete.as_view(create_field='nome'),
        name='categoria_pagamento-autocomplete',
    ),
    path(
        'projeto-autocomplete/',
        autocomplete.ProjetoAutocomplete.as_view(),
        name='projeto-autocomplete',
    ),
    path(
        'orcamento-autocomplete/',
        autocomplete.OrcamentoAutocomplete.as_view(),
        name='orcamento-autocomplete',
    ),
    path(
        'conta-bancaria-autocomplete/',
        autocomplete.ContaBancariaAutocomplete.as_view(),
        name='conta-bancaria-autocomplete',
    ),
]
