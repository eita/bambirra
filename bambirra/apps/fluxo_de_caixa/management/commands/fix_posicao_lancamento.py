from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Movimento


class Command(BaseCommand):

    def handle(self, *args, **options):

        lancamentos = Movimento.objects.all().order_by('data', 'id')

        count = 0
        for lancamento in lancamentos:
            lancamento.posicao_lancamento = \
                    int(lancamento.gerar_posicao_lancamento())

            count=count+1

            try:
                lancamento.save()
            except:
                import pdb; pdb.set_trace()

        self.stdout.write(self.style.SUCCESS("\nPosição dos lançamentos atualizados com sucesso!\n"))
