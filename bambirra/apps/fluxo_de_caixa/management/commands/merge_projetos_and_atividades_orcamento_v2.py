from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import (
        Projeto, Atividade, Orcamento,
        OrcamentoAtividade, Cliente)


class Command(BaseCommand):

    def handle(self, *args, **options):

        # Filtrar projetos relacionados a um termo "varal"; "rios"
        projetos_tomerge = ['icv', 'lume']

        for projeto_tomerge in projetos_tomerge:
            projetos = Projeto.objects\
                    .filter(slug__startswith=projeto_tomerge)\
                    .order_by('id')
                    # .exclude(slug__contains="iieb")\
                    # .exclude(slug__contains="graal")\

            # identificar primeiro projeto como principal
            projeto_ref = projetos.first()

            for projeto in projetos:
                if projeto == projeto_ref:
                    continue

                for orcamento in projeto.orcamentos.all():

                    for atividade in orcamento.atividades.all():
                        for retirada in atividade.retiradas.all():
                            retirada.projeto=projeto_ref
                            retirada.save()

                    orcamento.projeto = projeto_ref
                    orcamento.save()



        self.stdout.write(self.style.SUCCESS("\nProjetos, orçamentos & Atividades atualizados!\n"))
