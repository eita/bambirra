from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Projeto, Orcamento


class Command(BaseCommand):

    def handle(self, *args, **options):

        # lista de projetos com backup
        projetos_backup="..."

        projeto_correspondente = []
        projeto_sem_correspondencia = []
        projeto_mais_de_uma_correspondencia = []

        for proj_backup in projetos_backup.split(','):
            projeto = Projeto.objects.filter(nome__icontains=proj_backup)
            value = f"{proj_backup} -> "
            if projeto.count() == 1:
                projeto_obj = projeto[0]
                projeto_correspondente.append(f"{value} {projeto_obj}")

                projeto_obj.backup = True
                projeto_obj.backup_nome_identificacao = proj_backup
                projeto_obj.save()
            elif projeto.count() > 1:
                value += ', '.join([str(p) for p in projeto])
                projeto_mais_de_uma_correspondencia.append(value)
            else:
                # Tentar correspondência através do orçamento
                orcamento = Orcamento.objects.filter(nome__icontains=proj_backup)
                if not orcamento:
                    projeto_sem_correspondencia.append(value)
                elif orcamento.count() > 1:
                    value += ', '.join([str(o.projeto) for o in orcamento])
                    projeto_mais_de_uma_correspondencia.append(value + '( Orçamento )')
                else:
                    projeto_obj = orcamento[0].projeto
                    projeto_correspondente.append(f"{value} {projeto_obj} ( Orçamento )")

                    projeto_obj.backup = True
                    projeto_obj.backup_nome_identificacao = proj_backup
                    projeto_obj.save()


        print('')
        print('####################')
        print('# CORRESPONDÊNCIAS #')
        print('####################')
        for correspondencia in projeto_correspondente:
            print(correspondencia)

        print('')
        print('###############################')
        print('# MAIS DE UMA CORRESPONDENCIA #')
        print('###############################')
        for correspondencia in projeto_mais_de_uma_correspondencia:
            print(correspondencia)

        print('')
        print('#######################')
        print('# SEM CORRESPONDENCIA #')
        print('#######################')
        for correspondencia in projeto_sem_correspondencia:
            print(correspondencia)


        self.stdout.write(self.style.SUCCESS("\nProjetos atualizados com sucesso!\n"))
