from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Retirada


class Command(BaseCommand):

    def handle(self, *args, **options):

        retiradas = Retirada.objects.filter(cooperada_valor_hora__isnull=True)
        for r in retiradas:
            r.get_cooperada_valor_hora_retirada()
            print(f"Data ref.: { r.data_referencia },R$ { r.cooperada_valor_hora },{ r }")

        self.stdout.write(self.style.SUCCESS("\nValor/hora cooperada atualizada em todas retiradas!\n"))
