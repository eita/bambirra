from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Movimento


class Command(BaseCommand):

    def handle(self, *args, **options):

        lancamentos = Movimento.objects.all()\
                .order_by('data', 'posicao_lancamento')

        for lancamento in lancamentos:

            # checar se valor_tributo está atribuído
            if lancamento.valor_tributo:
                valor_bruto = lancamento.valor + lancamento.valor_tributo
                print("COM VALOR ATRIBÍDO")
                print(f"{lancamento.valor} + {lancamento.valor_tributo} = {valor_bruto}")
            else:
                # checar se tem tributos relacionados
                if lancamento.tributos.count():
                    print("COM TRIBUTOS RELACIONADOS")
                    print(f"{lancamento.valor} + {lancamento.valor_tributo} = calcular")
                else:
                    # print(f"valor_bruto = {lancamento.valor}")
                    valor_bruto = lancamento.valor

            lancamento.valor_bruto = valor_bruto
            lancamento.save()

        self.stdout.write(self.style.SUCCESS("\nValor bruto atribuído com sucesso!\n"))
