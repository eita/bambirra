from django.db import models
from django.db.models import Count, Sum
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Retirada, RetiradaProjeto


class Command(BaseCommand):

    def handle(self, *args, **options):
        lista_duplicados = []
        retiradas = Retirada.objects.all()
        for retirada in retiradas:
            retirada_projeto = RetiradaProjeto.objects.filter(
                retirada=retirada).values(
                    'retirada',
                    'projeto',
                    'projeto__nome').annotate(
                        projeto_count=Count('projeto__nome'),
                        horas_somadas=Sum('horas')).filter(
                            projeto_count__gt=1)
            if retirada_projeto:
                lista_duplicados.append(retirada_projeto[0])

        for item in lista_duplicados:
            kwargs = {
                'retirada_id': item['retirada'],
                'projeto_id': item['projeto'],
            }

            RetiradaProjeto.objects.filter(**kwargs).delete()

            kwargs['horas'] = item.get('horas_somadas', 0)

            novo_item = RetiradaProjeto(**kwargs)
            novo_item.save()

        self.stdout.write(self.style.SUCCESS("\nOs projetos em duplicação foram somados com sucesso!\n"))

