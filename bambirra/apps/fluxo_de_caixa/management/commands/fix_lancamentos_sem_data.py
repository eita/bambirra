from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Movimento


class Command(BaseCommand):

    def handle(self, *args, **options):

        lancto_no_bambirra_nao_na_planilha = {
            2316: 'nao-localizado',
            2315: 'nao-localizado',

            1464: 'nao-localizado',
            1448: 'nao-localizado',
            1446: 'nao-localizado',

            1426: 'nao-localizado',
        }

        # registros alem de 2012 setembro e outubro
        # numeros de linhas
        lancto_na_planilha_nao_do_bambirra = [
            411, # esta linha já existeno bambirra
            826, 859, 894, 923, 967, 1662, 1664, 1682, 2668, 2670,
        ]

        lancamentos = Movimento.objects.filter(data__isnull=True).order_by('-data', '-id')
        print('\nLançamentos sem data: %i' % lancamentos.count())

        for lancamento in lancamentos:
            if lancamento.id in data_lancamento:
                print('\n=== Lançamento sem data: %i | %f ===' % (lancamento.id, lancamento.valor))
                if lancamento.id == 2315:
                    lancto_anterior=Movimento.objects.get(id=lancamento.id-6)
                else:
                    lancto_anterior=Movimento.objects.get(id=lancamento.id-1)
                print('Lançamento anterior: %s' % lancto_anterior.data)
                lancto_posterior=Movimento.objects.get(id=lancamento.id+1)
                print('Lançamento posterior: %s' % lancto_posterior.data)

        self.stdout.write(self.style.SUCCESS("\nPosição dos lançamentos atualizados com sucesso!\n"))
