from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import OrcamentoAtividade


class Command(BaseCommand):

    def handle(self, *args, **options):

        atividades_orcamentos = OrcamentoAtividade.objects.all()
        for atividade_orcamento in atividades_orcamentos:
            encerramento = \
                    atividade_orcamento.orcamento.encerramento
            encerramento_previsto =\
                    atividade_orcamento.orcamento.encerramento_previsto
            if encerramento:
                atividade_orcamento.encerramento = encerramento
            if encerramento_previsto:
                atividade_orcamento.encerramento_previsto = encerramento_previsto
            atividade_orcamento.save()

        self.stdout.write(self.style.SUCCESS("\nData encerramento e encerramento previsto atribuidos nas atividades de orçamentos\n"))
