import re, csv, datetime
from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.template.defaultfilters import slugify

from fluxo_de_caixa.models import models


class Command(BaseCommand):

    help = 'Importar dados em TSV'

    def __init__(self):
        super().__init__()

        self.app_name = 'fluxo_de_caixa'
        self.foreign_keys = {}
        self.dict_projetos = {}

    def add_arguments(self, parser):
        parser.add_argument('file', nargs=1)
        parser.add_argument('projects', nargs=1)

    def handle(self, *args, **options):

        projects_file = options['projects'][0]
        self.get_projetos(projects_file)

        file = options['file'][0]

        self.projetos_error = []

        with open(file) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter="\t")
            for row in csv_reader:

                if not self.is_movimento(row[0]):
                    continue

                k = {}
                k['movimento_data'] = self.normalizar_data(row[0])
                k['conta_bancaria'] = self.get_object(
                    'ContaBancaria', row[1])
                k['saldo'] = row[2]
                k['origem'] = row[3]
                k['destino'] = row[4]
                k['categoria'] = row[5]

                k['descricao'] = row[6]
                k['despesa'] = row[8]
                k['receita'] = row[7]
                k['credito'] = row[9]
                k['reembolso_de_credito'] = row[10]
                k['horas_trabalhadas'] = row[11]
                k['horas_ferias_s_licenca'] = row[12] # TODO: verificar modelagem
                k['projetos'] = row[13]


                if k['movimento_data']:
                    movimento_data_compare = datetime.date(
                        *list(map(int, k['movimento_data'].split('-'))))
                    if (movimento_data_compare >= datetime.date(2020, 8, 1)
                            and movimento_data_compare < datetime.date(2020, 10, 1)
                            and k['categoria'] != 'Retirada'):
                        print(k['movimento_data'])
                        print(k['categoria'])
                        pass
                    else:
                        continue
                else:
                    continue

                if k['despesa']:
                    k['despesa'] = self.get_valor(k['despesa'])
                    self.add_despesa(k)
                if k['receita']:
                    self.add_receita(k)
                if k['credito']:
                    k['credito'] = self.get_valor(k['credito'])
                    self.update_credito(k)
                if k['reembolso_de_credito']:
                    reembolso = self.get_valor(k['reembolso_de_credito'])
                    k['credito'] = float(reembolso) * (-1)
                    self.update_credito(k)

        # validar todos os saldos
        self.validar_saldos()

        '''
        if self.projetos_error:
            import pdb; pdb.set_trace();
        '''

        self.stdout.write(self.style.SUCCESS("\nImportação finalizada com sucesso!\n"))

    def validar_saldos(self):
        contas = models.ContaBancaria.objects.all()
        for conta in contas:
            conta.validar_saldo()

    def update_credito(self, k):
        cooperada = self.get_cooperada(k['destino'])

        if not cooperada: return True

        creditos = cooperada.creditos_saldo if cooperada.creditos_saldo else 0
        cooperada.creditos_saldo = float(creditos) + float(k['credito'])
        cooperada.save()

    def set_projeto_error(self, projeto):

        if not projeto in self.projetos_error:
            self.projetos_error.append(projeto)

        return ''

    def get_projeto(self, projeto):
        projeto = projeto.strip().title()
        if projeto not in self.dict_projetos:
            self.set_projeto_error(projeto)

            return None

        projeto = self.dict_projetos[projeto]
        projeto = self.get_object('Projeto', projeto)

        return projeto

    def get_projetos(self, projects_file):
        if not projects_file: return True

        with open(projects_file) as tsvfile:

            tsv_reader = csv.reader(tsvfile, delimiter="\t")
            for row in tsv_reader:
                for cell in row:
                    if re.match('nome.*projeto', cell,
                                re.IGNORECASE):
                        break
                    cell_content = cell.split(',')
                    for item in cell_content:
                        self.dict_projetos[item.strip().title()] = row[0].strip()

    def is_movimento(self, date):
        if not date: return True

        return re.match(
            '(\d+)\/(\d+)\/(\d+)',
            date)

    def get_object(self, model, value):
        value = value.strip().title()
        if not value: return ''

        if model not in self.foreign_keys:
            self.foreign_keys[model] = {}

        if value in self.foreign_keys[model]:
            return self.foreign_keys[model][username]

        model = apps.get_model(self.app_name, model)
        obj, created = model.objects.get_or_create(
            nome=value)
        return obj

    def get_user(self, nome):
        nome = nome.strip().title()
        username = slugify(nome)
        passwd = f"provisoria_{username}"

        if not nome: return ''

        if 'users' not in self.foreign_keys:
            self.foreign_keys['users'] = {}

        if username in self.foreign_keys['users']:
            return self.foreign_keys['users'][username]

        try:
            user = User.objects.create_user(
                username, '', passwd)
            user.first_name = nome
            user.is_staff = True
            user.is_superuser = True
            user.save()
        except IntegrityError as e:
            user = User.objects.get(username=username)

        self.foreign_keys['users'][username] = user

        return user

    def get_cooperada(self, nome):

        if not nome: return ''

        if 'cooperada' not in self.foreign_keys:
            self.foreign_keys['cooperada'] = {}

        if nome in self.foreign_keys['cooperada']:
            return self.foreign_keys['cooperada'][nome]

        user = self.get_user(nome)

        try:
            cooperada = models.Cooperada()
            cooperada.user = user
            cooperada.save()
        except IntegrityError as e:
            cooperada = models.Cooperada.objects.get(user=user)

        self.foreign_keys['cooperada'][nome] = cooperada

        return cooperada

    def add_despesa(self, k):

        if self.is_retirada(k):
            self.add_retirada(k)
        else:
            self.add_pagamento(k)

    def is_retirada(self, k):
        return re.match(
            'retirada',
            f"{k['categoria']} {k['descricao']}",
            re.IGNORECASE)

    def add_retirada(self, k):

        retirada = models.Retirada()
        retirada.valor = k['despesa']
        retirada.descricao = k.get('descricao', '')
        horas_trabalhadas = self.get_valor(k.get('horas_trabalhadas', 0))
        if horas_trabalhadas:
            retirada.horas_trabalhadas = horas_trabalhadas
        cooperada = self.get_cooperada(k['destino'])
        if cooperada:
            retirada.cooperada = cooperada

        retirada.save()

        k['retirada'] = retirada

        self.add_retirada_projetos(k)
        self.add_retirada_quitacao(k)

    def get_valor(self, valor):
        return valor.strip().replace(
            '.', '').replace(
                ',', '.')

    def add_retirada_projetos(self, k):
        if not k['projetos']:
            return True

        projetos = self.normalizar_projetos(k['projetos'])
        projetos = projetos.split(';')
        retirada_horas_total = 0
        for projeto in projetos:
            item = projeto.split(':')
            projeto = self.get_projeto(item[0].strip().title())
            if not projeto: continue

            retirada_projeto = models.RetiradaProjeto()
            retirada_projeto.projeto = projeto
            retirada_projeto.retirada = k['retirada']
            if len(item) > 1:
                horas = self.normalizar_horas(item[1])
                try:
                    horas = float(re.sub('(\d+)\ .*', r'\1', horas))
                except:
                    horas = 0

                if horas:
                    retirada_projeto.horas = horas
                    retirada_horas_total += horas

            retirada_projeto.save()

        self.check_horas_retirada(retirada_horas_total, k)

    def check_horas_retirada(self, retirada_horas_total, k):
        horas_trabalhadas = self.normalizar_horas(k['horas_trabalhadas'])
        if len(horas_trabalhadas) and retirada_horas_total != float(horas_trabalhadas):
            k['retirada'].horas_trabalhadas_diff = retirada_horas_total
            k['retirada'].save()

    def normalizar_projetos(self, projetos):
        return re.sub(
            '([\dhH][\ ]*)(\,)([\ ]*[0-9]*[a-zA-Z]+)',
            r'\1;\3', projetos)

    def normalizar_horas(self, horas):
        horas = horas.strip().replace(',', '.')
        return re.sub(':$', '', horas.replace('h', ':'))

    def normalizar_data(self, data):
        return re.sub('(\d+)\/(\d+)\/(\d+)', r'\3-\2-\1', data)

    def add_retirada_quitacao(self, k):
        if not k['movimento_data']:
            return True

        k['retirada'].data = k['movimento_data']
        if k['conta_bancaria']:
            k['retirada'].conta = k['conta_bancaria']
        k['retirada'].quitado_em = k['movimento_data']
        k['retirada'].save()

        self.update_conta_saldo(k)

    def update_conta_saldo(self, k):
        saldo = k.get('saldo',0)
        if not saldo or not k['movimento_data']: return True

        k['conta_bancaria'].saldo_valor = self.get_valor(saldo)
        k['conta_bancaria'].saldo_atualizacao = k['movimento_data']
        k['conta_bancaria'].save()

    def add_pagamento(self, k):

        pagamento = models.Pagamento()
        pagamento.valor = k['despesa']
        if k['movimento_data']:
            pagamento.data = k['movimento_data']
            pagamento.quitado_em = k['movimento_data']

        pagamento.descricao = f"Destino: {k.get('destino', '')} | {k.get('descricao', '')}"

        if k['conta_bancaria']:
            pagamento.conta = k['conta_bancaria']

        categoria = self.get_object(
            'CategoriaPagamento', k['categoria'])
        if categoria:
            pagamento.categoria = categoria

        pagamento.destino = k['destino'].strip().title()

        pagamento.save()

        self.update_conta_saldo(k)

    def add_receita(self, k):
        entrada = models.Entrada()
        entrada.valor = self.get_valor(k['receita'])
        if k['movimento_data']:
            entrada.data = k['movimento_data']
        descricao = f"Origem: {k['origem']} | {k.get('descricao', '')}"
        entrada.descricao = descricao

        if k['conta_bancaria']:
            entrada.conta = k['conta_bancaria']

        if k['categoria']:
            entrada.categoria = self.get_object(
                'CategoriaEntrada', k['categoria'])

        if self.is_faturamento(k['categoria']):
            projeto = self.get_projeto(k['origem'].strip().title())

            if projeto:
                entrada.projeto = projeto

        entrada.save()

        self.update_conta_saldo(k)

    def is_faturamento(self, categoria):
        return re.match(
            'faturamento',
            f"{categoria}",
            re.IGNORECASE)

    def add_credito(self, k):
        categoria = self.get_object(
            'CategoriaEntrada', k['categoria'])

