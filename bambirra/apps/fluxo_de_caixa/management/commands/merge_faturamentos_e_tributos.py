import re
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Movimento


class Command(BaseCommand):

    def handle(self, *args, **options):

        lancamentos = Movimento.objects\
                .filter(data__year=2022)\
                .order_by('data', 'posicao_lancamento')

        faturamento_a_tributar = []
        faturamento_tributado = []
        tributo_a_incidir = []

        for lancamento in lancamentos:
            try:
                object = lancamento.saida.pagamento
            except:
                try:
                    object = lancamento.entrada
                except:
                    continue

            categoria = object.categoria.nome

            # checar se categoria é faturamento
            if 'Faturamento' in categoria:
                if not re.match(
                        '.*(nota\ \d+).*',
                        lancamento.descricao,
                        flags=re.IGNORECASE):
                    continue
                ## armazenar descrição
                faturamento_anterior = lancamento
                faturamento_anterior_descricao = lancamento.descricao
                faturamento_a_tributar\
                        .append({lancamento.id: lancamento.descricao})
                print(f'Faturamento: {lancamento.descricao}')

            # checar se categoria é imposto
            if 'Impostos' in categoria:
                descricao = lancamento.descricao
                ## checar numero da nota na descricao
                num_nota = re.sub(
                        '.*(nota\ \d+).*',
                        r'\1',
                        descricao,
                        flags=re.IGNORECASE)
                if num_nota in faturamento_anterior_descricao:
                    print(f'tratar caso faturamento_anterior_descricao {faturamento_anterior_descricao}')
                    self.merge(faturamento_anterior, lancamento)
                elif num_nota in str(faturamento_a_tributar):
                    print('tratar caso faturamento_a_tributar')
                else:
                    if re.match(
                            '.*(nota\ \d+).*',
                            num_nota,
                            flags=re.IGNORECASE):
                        print(f'tratar caso tributo_a_incidir {lancamento.id} {lancamento.descricao}')
                        tributo_a_incidir.append({lancamento.id: lancamento.descricao})

    def merge(self, faturamento, tributo):
        valor = faturamento.valor
        valor_tributo = tributo.valor
        if valor_tributo < 0:
            valor_tributo = valor_tributo * (-1)
        valor_liquido = valor - valor_tributo
        if not faturamento.valor_tributo:
            faturamento.valor = valor_liquido
            faturamento.valor_tributo = valor_tributo
            faturamento.save()

            # excluir registro do tributo
            tributo.delete()
