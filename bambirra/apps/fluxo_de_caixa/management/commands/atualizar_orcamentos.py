from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Orcamento


class Command(BaseCommand):

    def handle(self, *args, **options):

        # projetos = Projeto.objects.all()
        # for projeto in projetos:
        #     projeto.atualizar_orcamento()

        orcamentos = Orcamento.objects.all()

        for orcamento in orcamentos:
            orcamento.atualizar_orcamento_horas_utilizadas()

        self.stdout.write(self.style.SUCCESS("\nOrçamentos atualizados com sucesso!\n"))
