from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import (
        Projeto, Atividade, Orcamento,
        OrcamentoAtividade, Cliente)


class Command(BaseCommand):

    def handle(self, *args, **options):

        # Filtrar projetos relacionados a um termo "varal"; "rios"
        projetos_tomerge = ['varal', 'rios']

        for projeto_tomerge in projetos_tomerge:
            projetos = Projeto.objects\
                    .filter(slug__startswith=projeto_tomerge)\
                    .order_by('id')
                    # .exclude(slug__contains="iieb")\
                    # .exclude(slug__contains="graal")\

            # identificar primeiro projeto como principal
            projeto_ref = projetos.first()

            # encontrar orçamento com retiradas
            orcamentos_projeto_ref = projeto_ref.orcamentos.all()
            if orcamentos_projeto_ref.count() != 1:
                print('Orçamento referencia não encontrado!')
                return
            orcamento_ref = orcamentos_projeto_ref.first()

            for projeto in projetos:
                if projeto == projeto_ref:
                    continue

                if projeto.slug in ['varal-iieb-canvas', 'rios-graal-brasil']:
                    # criar novo cliente
                    novo_cliente_nome = ' '.join(projeto.nome.split(' ')[1:])
                    if 'iieb' in projeto.slug:
                        novo_cliente_nome = 'IEB'
                    novo_cliente_slug = slugify(novo_cliente_nome)
                    novo_cliente, created = Cliente\
                            .objects.get_or_create(
                                    nome = novo_cliente_nome,
                                    )
                    novo_cliente.slug = novo_cliente_slug
                    novo_cliente.save()

                    # Criar orçamento para projeto_ref,
                    # com cliente atribuído como sufixo do nome
                    novo_orcamento, created = Orcamento\
                            .objects.get_or_create(
                                    projeto=projeto_ref,
                                    cliente=novo_cliente
                                    )
                    # com atividade padrão
                    # Criar orcamento_atividade com atividade do novo orçamento
                    atividade_padrao = Atividade.objects.get(slug='nao-especificada')
                    orcamento_atividade, created = novo_orcamento\
                            .atividades.get_or_create(
                                    atividade=atividade_padrao,
                                    )

                    # Relacionar novo orcamento_atividade as retiradas relacionadas
                    # Retiradas
                    retiradas = projeto\
                            .orcamentos.last()\
                            .atividades.first()\
                            .retiradas.all()

                    for retirada in retiradas:
                        retirada.projeto = projeto_ref
                        retirada.orcamento_atividade = orcamento_atividade
                        retirada.save()

                else:
                    pass

                    # Criar uma atividade no orçamento_ref com o sufixo do nome do projeto
                    nova_atividade_nome = ' '.join(projeto.nome.split(' ')[1:])
                    nova_atividade_slug = slugify(nova_atividade_nome)
                    nova_atividade, created = Atividade.objects.get_or_create(
                            nome = nova_atividade_nome,
                            slug = nova_atividade_slug
                            )

                    # Relaciona nova_atividade com orçamento_ref
                    nova_atividade_orcamento, created = OrcamentoAtividade\
                            .objects.get_or_create(
                                    orcamento=orcamento_ref,
                                    atividade=nova_atividade
                                    )

                    # Altera a atividade_orcamento da retirada_projeto relacionada a
                    # atividade_orcamento relacionado ao projeto, atribuindo a nova_atividade_orcamento

                    # Retiradas
                    retiradas = projeto\
                            .orcamentos.first()\
                            .atividades.first()\
                            .retiradas.all()

                    for retirada in retiradas:
                        retirada.projeto = projeto_ref
                        retirada.orcamento_atividade = nova_atividade_orcamento
                        retirada.save()

        self.stdout.write(self.style.SUCCESS("\nProjetos, orçamentos & Atividades atualizados!\n"))
