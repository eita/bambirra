from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import (Atividade, Orcamento)


class Command(BaseCommand):

    def handle(self, *args, **options):

        # Obter atividade padrão
        atividade_padrao, created = Atividade.objects\
                .get_or_create(
                        nome='Não especificada',
                        slug='nao-especificada')

        # relacionar atividade padrão ao orçamento
        orcamentos = Orcamento.objects.all()
        for orcamento in orcamentos:
            retiradas = orcamento.projeto.retiradas.all()

            if not retiradas:
                continue

            orcamento_atividade, created = orcamento\
                    .atividades.get_or_create(
                            atividade=atividade_padrao)
            orcamento_atividade.qtd_horas = orcamento.horas
            orcamento_atividade.save()

            # relacionar atividade padrão a retirada
            for retirada in retiradas:
                retirada.orcamento_atividade = orcamento_atividade
                try:
                    retirada.save()
                except IntegrityError as e:
                    print(f"IntegrityError: {retirada} | {orcamento_atividade} | {orcamento}")
                    pass
                    # import pdb; pdb.set_trace()

        self.stdout.write(self.style.SUCCESS("\nAtividade padrão atribuída com sucesso!\n"))
