from django.core.management.base import BaseCommand, CommandError

from fluxo_de_caixa.utils import gerar_livro_caixa

class Command(BaseCommand):

    def add_arguments(self, parser):
        # parser.add_argument('export_path', nargs=1)
        parser.add_argument('year', nargs=1)
        parser.add_argument('month', nargs=1)

    def handle(self, *args, **options):

        # export_path = options['export_path'][0]
        year = options['year'][0]
        month = options['month'][0]

        livro_caixa = gerar_livro_caixa(month, year)

        print(f"Livro caixa: { livro_caixa }")
