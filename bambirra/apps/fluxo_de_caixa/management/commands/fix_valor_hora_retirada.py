import re, csv
import decimal
from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.template.defaultfilters import slugify

from fluxo_de_caixa.models import models


class Command(BaseCommand):

    help = 'Importar dados em TSV'

    def __init__(self):
        super().__init__()

        self.app_name = 'fluxo_de_caixa'
        self.foreign_keys = {}
        self.dict_projetos = {}

    def handle(self, *args, **options):

        retiradas = models.Retirada.objects.all()\
                .order_by('data', 'posicao_lancamento')
        for retirada in retiradas:
            valor_retirada = 0
            if retirada.cooperada_valor_hora and retirada.horas_trabalhadas:
                valor_retirada = retirada.cooperada_valor_hora * retirada.horas_trabalhadas
                valor_retirada = valor_retirada * (-1)

                valores_possiveis = [
                        (retirada.horas_trabalhadas * 20) * (-1),
                        (retirada.horas_trabalhadas * 25) * (-1),
                        (retirada.horas_trabalhadas * 30) * (-1),
                        (retirada.horas_trabalhadas * 35) * (-1),
                        ]

                if valor_retirada != retirada.valor:
                    cooperada_valor_hora_retirada = retirada.get_cooperada_valor_hora_retirada(recalcular=True)
                    print(f"{retirada.data} | {retirada.cooperada_valor_hora} * {retirada.horas_trabalhadas} = {valor_retirada} <> {retirada.valor} | {retirada.cooperada} | {retirada.descricao}")
                    # print(f"{cooperada_valor_hora_retirada}")

                    if decimal.Decimal(str(retirada.valor)) in valores_possiveis:
                        try:
                            retirada.valor = (decimal.Decimal(str(cooperada_valor_hora_retirada)) * retirada.horas_trabalhadas) * (-1)
                        except:
                            import pdb; pdb.set_trace()

                    retirada.cooperada_valor_hora = cooperada_valor_hora_retirada
                    retirada.save()

        self.stdout.write(self.style.SUCCESS("\nImportação finalizada com sucesso!\n"))

    def get_valor(self, valor):
        return valor.strip().replace(
            '.', '').replace(
                ',', '.')

    def normalizar_horas(self, horas):
        horas = horas.strip().replace(',', '.')
        return re.sub(':$', '', horas.replace('h', ':'))
