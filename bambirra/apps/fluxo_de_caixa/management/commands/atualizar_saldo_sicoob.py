from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Movimento, ContaBancaria


class Command(BaseCommand):

    def handle(self, *args, **options):

        # TODO
        # Quando essa atualizacao deve acontecer ?

        # Obter conta SICOOB
        conta = ContaBancaria.objects.get(nome__iexact='sicoob')
        conta.atualizar_saldo_lancamentos()

        self.stdout.write(self.style.SUCCESS("\nSaldo conta SICOOB atualizado com sucesso!\n"))
