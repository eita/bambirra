from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Entrada


class Command(BaseCommand):

    def handle(self, *args, **options):

        faturamentos = Entrada.objects.all()
        for faturamento in faturamentos:
            projeto = faturamento.projeto
            data = faturamento.data
            if not projeto or not data:
                continue

            orcamentos = projeto.orcamentos.all()
            if orcamentos.count() == 1:
                orcamento = orcamentos.first()
            else:
                # Obter orcamento com data_aprovação < faturamento.data
                # Ordenado pela data_aprovação decrescente
                # Obter o primeiro registro
                orcamento = orcamentos\
                        .filter(aprovado_em__lte=faturamento.data)\
                        .order_by('-aprovado_em')\
                        .first()

            faturamento.orcamento = orcamento
            faturamento.save()

        self.stdout.write(self.style.SUCCESS("\nOrçamento atribuído no faturamento\n"))
