from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Projeto, Atividade, Orcamento


class Command(BaseCommand):

    def handle(self, *args, **options):

        projeto_slug_prev = ''
        orcamentos = Orcamento.objects.all()\
                .order_by('projeto__slug', 'aprovado_em')
        for orcamento in orcamentos:
            projeto = orcamento.projeto
            if projeto.slug == projeto_slug_prev:
                orcamento_nome += 1
            else:
                orcamento_nome = 1

            projeto_slug_prev = projeto.slug

            orcamento.nome = f"{orcamento_nome}"
            orcamento.slug = f"{orcamento_nome}"
            orcamento.save()

        self.stdout.write(self.style.SUCCESS("\nNomes atribuidos em orçamento\n"))
