from django.db import models
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Retirada, RetiradaProjeto


class Command(BaseCommand):

    def handle(self, *args, **options):

        retirada_projetos = RetiradaProjeto.objects.filter(
            horas_trabalhadas_textual__isnull=True)

        for item in retirada_projetos:
            if not item.horas:
                continue

            item.horas_trabalhadas_textual = str(item.horas).replace('.', ',')
            item.save()

        self.stdout.write(self.style.SUCCESS("\nHoras trabalhadas textual corrigidas com sucesso!\n"))

