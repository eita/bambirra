from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Projeto, Atividade, Orcamento


class Command(BaseCommand):

    def handle(self, *args, **options):

        orcamentos = Orcamento.objects.all()
        for orcamento in orcamentos:
            if orcamento.cliente:
                continue

            cliente = orcamento.projeto.cliente
            if not cliente:
                continue

            orcamento.cliente = cliente
            orcamento.save()

        self.stdout.write(self.style.SUCCESS("\nClientes atribuidos em orçamento\n"))
