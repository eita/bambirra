from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import ContaBancaria


class Command(BaseCommand):

    def handle(self, *args, **options):

        contas = ContaBancaria.objects.all()

        for conta in contas:
            try:
                data_ultimo_lancamento = conta.movimentos.filter().order_by('-posicao_lancamento').first().data
            except:
                data_ultimo_lancamento = '2000-01-01'
            conta.data_ultimo_lancamento = data_ultimo_lancamento
            conta.save()

        self.stdout.write(self.style.SUCCESS("\nConta bancária: Datas de último lançamento atualizados com sucesso!\n"))
