import re, csv
from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.template.defaultfilters import slugify

from fluxo_de_caixa.models import models


class Command(BaseCommand):

    help = 'Importar dados em TSV'

    def __init__(self):
        super().__init__()

        self.app_name = 'fluxo_de_caixa'
        self.foreign_keys = {}
        self.dict_projetos = {}

    def add_arguments(self, parser):
        parser.add_argument('file', nargs=1)
        parser.add_argument('projects', nargs=1)

    def handle(self, *args, **options):

        projects_file = options['projects'][0]

        file = options['file'][0]

        self.projetos_error = []

        historico_horas = []

        with open(file) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter="\t")
            for row in csv_reader:
                data = row[0]
                cooperada = row[4]
                categoria = row[5]
                descricao = row[7]
                despesa = row[9]
                horas_trab = row[12]

                if ('retirada' not in slugify(categoria)
                        or not horas_trab
                        or not despesa):
                    continue

                despesa = self.get_valor(despesa)
                horas_trab = self.normalizar_horas(horas_trab)
                valor_hora = float(despesa) / float(horas_trab)

                # if valor_hora in historico_horas:
                #     continue

                historico_horas.append(valor_hora)
                print(f"{data} | {valor_hora} | {cooperada} | {descricao}")

        self.stdout.write(self.style.SUCCESS("\nImportação finalizada com sucesso!\n"))

    def get_valor(self, valor):
        return valor.strip().replace(
            '.', '').replace(
                ',', '.')

    def normalizar_horas(self, horas):
        horas = horas.strip().replace(',', '.')
        return re.sub(':$', '', horas.replace('h', ':'))
