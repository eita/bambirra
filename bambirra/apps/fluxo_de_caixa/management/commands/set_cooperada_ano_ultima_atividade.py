from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Cooperada


class Command(BaseCommand):

    def handle(self, *args, **options):

        for cooperada in Cooperada.objects.all():
            cooperada.ano_ultima_atividade = cooperada.get_ano_ultima_atividade()
            print(cooperada.ano_ultima_atividade)
            cooperada.save()

        self.stdout.write(self.style.SUCCESS("\nCooperadas atualizadas com sucesso!\n"))
