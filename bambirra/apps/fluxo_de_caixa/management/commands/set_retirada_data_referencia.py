import datetime
from dateutil.relativedelta import relativedelta
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Retirada


class Command(BaseCommand):

    def handle(self, *args, **options):

        for retirada in Retirada.objects.all():
            data_quitacao = retirada.data
            if not data_quitacao:
                data_quitacao = datetime.date.today()

            data_referencia = data_quitacao + relativedelta(months=-1)
            data_referencia = data_referencia.strftime("%Y-%m-01")

            retirada.data_referencia = data_referencia
            retirada.save()

        self.stdout.write(self.style.SUCCESS("\nData de referência das retiradas atribuídas com sucesso!\n"))
