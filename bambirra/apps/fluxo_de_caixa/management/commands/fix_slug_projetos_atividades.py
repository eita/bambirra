from django.utils.text import slugify
from django.db.utils import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Projeto, Atividade


class Command(BaseCommand):

    def handle(self, *args, **options):

        for projeto in Projeto.objects.all():
            projeto.slug=slugify(projeto.nome)
            try:
                projeto.save()
            except IntegrityError as e:
                print(f"Projeto duplicado {projeto}")
                pass

        for atividade in Atividade.objects.all():
            atividade.slug=slugify(atividade.nome)
            atividade.save()

        self.stdout.write(self.style.SUCCESS("\nProjetos & Atividades slugify!\n"))
