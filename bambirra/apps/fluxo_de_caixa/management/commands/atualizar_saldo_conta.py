from django.core.management.base import BaseCommand, CommandError
from fluxo_de_caixa.models.models import Movimento, ContaBancaria


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('conta', nargs=1)

    def handle(self, *args, **options):
        conta_arg = options['conta'][0]

        conta = ContaBancaria.objects.get(nome__iexact=conta_arg)
        conta.atualizar_saldo_lancamentos()

        self.stdout.write(self.style.SUCCESS(
            f"\nSaldo conta {conta_arg} atualizado com sucesso!\n"))
