from django.conf import settings
from django.urls import include, path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from fluxo_de_caixa.admin import admin_site

urlpatterns = [

    # Adding a password reset feature
    path('admin/password_reset/',
        auth_views.PasswordResetView.as_view(),
        name='admin_password_reset',),
    path('admin/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done',),
    path('reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',),
    path('reset/done/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete',),

    # Text editor
    path('summernote/', include('django_summernote.urls')),

    # Admin site
    path('administrativo/',include('fluxo_de_caixa.urls')),

    # path('', TemplateView.as_view(template_name="base.html")),
]

urlpatterns += static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT)

urlpatterns += [
    path('', admin_site.urls),
]

# Add urls from static files when environment is local
# (not run collectstatic in local env)
urlpatterns += staticfiles_urlpatterns()
if settings.DEBUG:

    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
