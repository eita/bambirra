/* DIREITOS: FÉRIAS */

/*
Férias: 7 dias (ex.)
on keyup qtde. de dias de férias, retornar:
-----------------------------------------------------
ano  | saldo anterior | saldo atualizado | valor/hora
-----------------------------------------------------
2022 | 3              | cumprida!        | 35
2023 | 15             | 11               | 35
2024 | 30             | 30               | 37
*/

var DIAS_PREV=null;
function get_ferias_saldo () {
    dias = $('#id_dias_ferias').val();

    if (dias == DIAS_PREV){
        return;
    }
    DIAS_PREV = dias;

    if (! dias || dias==0) {
        hide_ferias_saldo();
        return;
    }

    usuaria = $('#id_cooperada').val();
    if (! usuaria || usuaria==0) {
        alert('Por favor, selecione a cooperada.');
        hide_ferias_saldo();
        return;
    }

    DATA_REFERENCIA = $('#id_data_referencia').val();
    if (! DATA_REFERENCIA) {
        alert('Por favor, selecione a data de referência.');
        hide_ferias_saldo();
        $('#id_dias_ferias').val(null);
        $('#id_data_referencia').focus();
        return;
    }

    ARR_DATA_REF = DATA_REFERENCIA.split('/');
    RETIRADA_ANO = ARR_DATA_REF[2]

    params = {
        usuaria: usuaria,
        dias: dias,
        retirada_id: RETIRADA_ID,
        retirada_ano: RETIRADA_ANO
    }

    $.ajax({
        method: "GET",
        url: URL_FERIAS_SALDO,
        data: params
    })
        .done(function ( data ) {
            if (data.status==200) {
                show_ferias_saldo(data.ferias_saldo);
                $('#media_horas_ferias').html(data.media_horas_ferias);
            }
            else if (data.status==204) {
                console.log(data);
                alert(data.content);
                $('#id_dias_ferias').val(null);
                hide_ferias_saldo();
            }
            else {
                console.log('Erro ao obter o status de férias da usuária selecionada!');
                console.log(data);
            }
        });
}

function hide_ferias_saldo() {
    $('div.ferias_saldo:visible').remove();
}

function show_ferias_saldo(ferias_saldo) {
    div_ferias = $('div.ferias_saldo');
    dias_ferias_help_text = $('#id_dias_ferias').next('.help');

    // Mostra quadro de saldo
    if ($('div.ferias_saldo', dias_ferias_help_text).is(":hidden")
        || (!$('div.ferias_saldo', dias_ferias_help_text).length)) {
        dias_ferias_help_text.append(div_ferias.clone());
        $('div.ferias_saldo', dias_ferias_help_text).css('display', 'block');
    }

    // clean tbody and load data on tbody
    var tr_ferias_saldo = '';
    $.each(ferias_saldo, function(ano, arr_saldo) {
        tr_ferias_saldo += '<tr class="form-row" style="text-align: right;">';
        tr_ferias_saldo += '<td>'+ano+'</td>';
        tr_ferias_saldo += '<td>'+arr_saldo.saldo_anterior+'</td>';
        tr_ferias_saldo += '<td>'+arr_saldo.saldo_atualizado+'</td>';
        tr_ferias_saldo += '<td>'+arr_saldo.valor_hora+'</td>';
        tr_ferias_saldo += '</tr>';
    });
    $('tbody.ferias_saldo').html(tr_ferias_saldo);
}

get_ferias_saldo();
$('#id_dias_ferias')
    .on('keyup', function(){
        get_ferias_saldo();
        get_horas_descanso();
    })
    .on('blur', function(){
        get_ferias_saldo();
        get_horas_descanso();
    });

$('#id_cooperada')
    .on('change', function(){
        get_ferias_saldo();
        get_horas_descanso();
    });

$('#id_data_referencia')
    .on('blur', function(){
        DIAS_PREV=null
        get_ferias_saldo();
        // get_horas_descanso();
    });
