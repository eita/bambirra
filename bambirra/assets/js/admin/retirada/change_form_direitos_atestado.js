/*
    DIREITOS: CÁLCULO ATESTADO
    */

//////////
// EVENTS
//////////

// set init dias_atestado
$('#id_dias_atestado').val(DIAS_ATESTADO);

// get horas atestado
get_horas_atestado()
$('#id_dias_atestado')
    .on('keyup', get_horas_atestado);
$('#id_data_referencia')
  .on('blur', function(){
    DIAS_ATESTADO_PREV=null;
    get_horas_atestado();
  });

// update horas atestado
$('form').on('submit', update_horas_atestado);

/////////////
// CALLBACKS
/////////////

var HORAS_ATESTADO = null;
var DIAS_ATESTADO_PREV = null;
function get_horas_atestado () {
    // verificar se dias_atestado está preenchido
    dias_atestado = $('#id_dias_atestado').val()
    if (dias_atestado == DIAS_ATESTADO_PREV){
        return
    }
    DIAS_ATESTADO_PREV = dias_atestado;

    if (! dias_atestado) {
        // TODO retirar inline atestado.
        return
    }

    usuaria_id = $('#id_cooperada').val();
    data_retirada = $('#id_data_referencia').val();
    data_retirada = data_retirada.split('/');
    data_retirada = data_retirada[2]+'-'+data_retirada[1]+'-'+data_retirada[0];
    HORAS_ATESTADO = null;

    $.ajax({
        method: "GET",
        url: URL_GET_HORAS_ATESTADO,
        data: {
            usuaria_id: usuaria_id,
            dias_atestado: dias_atestado,
            data_retirada: data_retirada,
        }
    }).done(
        function ( data ) {
            if (data.status==200) {
                id_dias_atestado = $('#id_dias_atestado');

                // Help text:
                media_horas_atestado = data.media_horas_atestado.replace('.', ',');
                help_text = 'Média de horas ref. ao período a partir de '+ data.periodo_ref_calculo +': <b>' + media_horas_atestado + '</b>';

                dias_atestado_help_text = id_dias_atestado.next('.help');
                dias_atestado_help_text.html(help_text);

                HORAS_ATESTADO = data.horas_atestado
                set_inline_atestado(data);
            }
            else {
                console.log('Erro ao obter as horas para cálculo de atestado da usuária selecionada!');
            }
        });
}

var ID_INLINE_ATESTADO=null;
function set_inline_atestado(data) {

    // Montar inline somente na adição de atestado na retirada
    console.log('DIAS_ATESTADO');
    console.log(DIAS_ATESTADO);
    if (! DIAS_ATESTADO) {

        // verificar se o inline já existe. Se não, criar!
        if (! $('#inline_atestado').length) {
            TOTAL_FORMS = $('#id_projetos-TOTAL_FORMS').val();
            ID_INLINE_ATESTADO = parseInt(TOTAL_FORMS);

            // Atualizar id_projetos-TOTAL_FORMS
            $('#id_projetos-TOTAL_FORMS').val(ID_INLINE_ATESTADO+1);

            // criar inline
            input_hidden = '<input type="hidden" name="projetos-'+ID_INLINE_ATESTADO;
            inline = '<div id="inline_atestado">';
            inline += input_hidden+'-id" id="id_projetos-'+ID_INLINE_ATESTADO +'-id">';
            inline += input_hidden+'-retirada" id="id_projetos-'+ID_INLINE_ATESTADO +'-retirada" value="'+RETIRADA_ID+'">';
            inline += input_hidden+'-projeto" id="id_projetos-'+ID_INLINE_ATESTADO +'-projeto" value="'+PROJETO_ATESTADO+'">';
            inline += input_hidden+'-orcamento_atividade" id="id_projetos-'+ID_INLINE_ATESTADO +'-orcamento_atividade" value="'+ORCAMENTO_ATIVIDADE_ATESTADO+'">';
            inline += input_hidden+'-horas_trabalhadas_textual" id="id_projetos-'+ID_INLINE_ATESTADO +'-horas_trabalhadas_textual" class="horas_atestado_inline">';
            inline += '</div>';

            $('.field-dias_atestado').append(inline);
        }
        $('#id_projetos-'+ID_INLINE_ATESTADO+'-horas_trabalhadas_textual').val(data.horas_atestado);
    }
}

// Atualizar o valor do atestado via ajax
function update_horas_atestado (evt) {
    evt.preventDefault();

    // Verificar se é uma adição pelo retirada_id
    if (! DIAS_ATESTADO) {
        $(this).unbind().submit();
        return;
    }
    dias_atestado = $('#id_dias_atestado').val()
    if (DIAS_ATESTADO == dias_atestado) {
        $(this).unbind().submit();
        return;
    }

    $(document.body).css({'cursor' : 'wait'});
    $('input[type="submit"]').attr({'disabled' : 'disabled'});
    horas_atestado = HORAS_ATESTADO;
    if (horas_atestado === '') {
        horas_atestado = 0;
    }

    data = {
        horas_atestado: horas_atestado,
        retirada_id: RETIRADA_ID,
        orcamento_atividade_atestado: ORCAMENTO_ATIVIDADE_ATESTADO,
        projeto_atestado: PROJETO_ATESTADO
    }

    $.ajax({
        method: "GET",
        url: URL_UPDATE_HORAS_ATESTADO,
        data: data
    }).done(function(data_return){
        if (data_return.status==200) {
            $('form').unbind().submit();
        }
        else{
            console.log('Houve algum erro ao salvar as horas de atestado.');
        }
    });
}
