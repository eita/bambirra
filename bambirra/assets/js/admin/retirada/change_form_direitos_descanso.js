/*
    DIREITOS: CÁLCULO DESCANSO
    */
set_horas_descanso = function ( data ) {
    if (data.status==200) {
        id_horas_descanso = $('#id_horas_descanso');

        // Help text:

        // * Cálculo das horas de descanso
        help_text = 'Cálculo das horas de descanso.'
        help_text_changed = false;

        // caso seja uma alteração; já tiver atribuído horas de descanso; caso atribuição tenha sido manual
        if ((RETIRADA_ID && HORAS_DESCANSO_ORIGINAL && data.horas_descanso && data.horas_descanso != HORAS_DESCANSO_ORIGINAL)
            && (DESCANSO_EDIT_MANUAL==null || DESCANSO_EDIT_MANUAL==true)) {
            id_horas_descanso.val(HORAS_DESCANSO_ORIGINAL);

            help_text = 'Você tem direito a <b>'+data.horas_descanso+'</b> horas de descanso para <b>'+data.total_horas_atividades+'</b> horas trabalhadas. ';
            help_text += '<br/>Porém, este valor foi editado manualmente.';
            help_text += '<br/>Para voltar ao cálculo automático ';
            help_text += '<a style="cursor:pointer; text-decoration: underline" data-horas-descanso='+data.horas_descanso+' class="btn-descanso-auto-restart">clique aqui</a>';

            DESCANSO_EDIT_MANUAL = true;
            help_text_changed = true;
        }
        else {
            id_horas_descanso.val(data.horas_descanso);

            if (data.total_horas_atividades) {
                // * Horas de descanso integral para X horas trabalhadas no período.
                // * Horas de descanso proporcional para X horas trabalhadas no período.
                help_text = 'Horas de descanso <b>'+data.descanso+'</b> para <b>'+data.total_horas_atividades+'</b> horas trabalhadas no período.';
                help_text_changed = true;
            }

            DESCANSO_EDIT_MANUAL = false;
        }

        dias_ferias = $('#id_dias_ferias').val();
        if (dias_ferias > 0) {
            help_text_ferias = 'Descanso <b>proporcional</b> a qtde. de dias trabalhados, considerando suas <b>férias</b>.<br>';
            if (help_text_changed) {
                help_text = help_text_ferias + help_text;
            }
            else {
                help_text = help_text_ferias;
            }
        }

        horas_descanso_help_text = id_horas_descanso.next('.help');
        horas_descanso_help_text.html(help_text);

        set_inline_descanso(data)
    }
    else {
        console.log('Erro ao obter as horas para cálculo de descanso da usuária selecionada!');
    }
}

// restart auto calc descanso
$(document).on('click', '.btn-descanso-auto-restart', function(evt){
    horas_descanso = $(this).data('horas-descanso');
    $('#id_horas_descanso').val(horas_descanso);
});

var ID_INLINE_DESCANSO=null;
function set_inline_descanso(data) {
    // verificar se o inline já existe. Se não, criar!
    if (! $('#inline_descanso').length) {

        // Montar inline somente na adição de retirada
        // if (! RETIRADA_ID) {
        if (! HORAS_DESCANSO_ORIGINAL) {

            TOTAL_FORMS = $('#id_projetos-TOTAL_FORMS').val();
            ID_INLINE_DESCANSO = parseInt(TOTAL_FORMS);

            // Atualizar id_projetos-TOTAL_FORMS
            $('#id_projetos-TOTAL_FORMS').val(ID_INLINE_DESCANSO+1);

            // criar inline
            input_hidden = '<input type="hidden" name="projetos-'+ID_INLINE_DESCANSO;
            inline = '<div id="inline_descanso">';
            inline += input_hidden+'-id" id="id_projetos-'+ID_INLINE_DESCANSO +'-id">';
            inline += input_hidden+'-retirada" id="id_projetos-'+ID_INLINE_DESCANSO +'-retirada">';
            inline += input_hidden+'-projeto" id="id_projetos-'+ID_INLINE_DESCANSO +'-projeto" value="'+PROJETO_DESCANSO+'">';
            inline += input_hidden+'-orcamento_atividade" id="id_projetos-'+ID_INLINE_DESCANSO +'-orcamento_atividade" value="'+ORCAMENTO_ATIVIDADE_DESCANSO+'">';
            inline += input_hidden+'-horas_trabalhadas_textual" id="id_projetos-'+ID_INLINE_DESCANSO +'-horas_trabalhadas_textual" class="horas_descanso_inline">';
            inline += '</div>';

            $('.field-horas_descanso').append(inline);
        }
    }

    $('#id_projetos-'+ID_INLINE_DESCANSO+'-horas_trabalhadas_textual').val(data.horas_descanso);
}

HORAS_ATIVIDADES_PREV = null;
function get_horas_descanso () {

    horas_atividades = '';
    $('input[name$="-horas_trabalhadas_textual"]').each(function(){
        horas_atividade = $(this).val()
        if (! horas_atividade ) return

        if ($(this).hasClass('horas_descanso_inline')) return
        if ($(this).hasClass('horas_atestado_inline')) return

        tr = $(this).parents('tr')
        delete_element = tr.find('input[name$="-DELETE"]');
        if (delete_element.length) {
            delete_checked = delete_element[0].checked;
            if (delete_checked) return
        }

        if (horas_atividades) {
            horas_atividades = horas_atividades + '|' + horas_atividade
        }
        else {
            horas_atividades = horas_atividade
        }
    });

    if (! horas_atividades) {
        set_horas_descanso({horas_descanso: '', status: 200})
        return 0;
    }

    if (horas_atividades == HORAS_ATIVIDADES_PREV) {
        return;
    }
    HORAS_ATIVIDADES_PREV = horas_atividades

    // Considerar férias no descanso
    dias_ferias = $('#id_dias_ferias').val();
    usuaria_id = $('#id_cooperada').val();

    $.ajax({
        method: "GET",
        url: URL_GET_HORAS_DESCANSO,
        data: {
            horas_atividades: horas_atividades,
            dias_ferias: dias_ferias,
            usuaria_id: usuaria_id
        }
    }).done(set_horas_descanso);
}

function update_horas_descanso (evt) {
    evt.preventDefault();

    form_elem = $(this);

    // Verificar se é uma adição pelo retirada_id
    if (! RETIRADA_ID){
        form_elem.unbind().submit();
        return;
    }
    horas_descanso = $('#id_horas_descanso').val();
    if (HORAS_DESCANSO_ORIGINAL == horas_descanso) {
        form_elem.unbind().submit();
        return;
    }

    $(document.body).css({'cursor' : 'wait'});
    $('input[type="submit"]').attr({'disabled' : 'disabled'});
    if (horas_descanso === '') {
        horas_descanso = 0;
    }

    data = {
        horas_descanso: horas_descanso,
        retirada_id: RETIRADA_ID,
        orcamento_atividade_descanso: ORCAMENTO_ATIVIDADE_DESCANSO,
        projeto_descanso: PROJETO_DESCANSO
    }

    $.ajax({
        method: "GET",
        url: URL_UPDATE_HORAS_DESCANSO,
        data: data
    }).done(function(data_return){
        if (data_return.status==200) {
            form_elem.unbind().submit();
        }
        else{
            console.log('Houve algum erro ao salvar as horas de descanso.');
        }
    });
}

get_horas_descanso();
$(document)
    .on('keyup', 'input[name$="-horas_trabalhadas_textual"]', get_horas_descanso)
    .on('change', 'input[name$="-DELETE"]', get_horas_descanso)
    .on('click', '.inline-deletelink', get_horas_descanso);


$( document ).ready(function() {
  // Atualizar o valor do descanso via ajax
  $('form').on('submit', update_horas_descanso);
});
