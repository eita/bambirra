$(document).ready(function() {

    function atualizar_saldo(valor_liquido) {
        valor_liquido = parseFloat(valor_liquido)
        saldo = parseFloat(conta_saldo_lancamento_anterior) + valor_liquido ;
        $('#id_valor').val(valor_liquido.toFixed(2))
        $('#calculo-valor_liquido').html(valor_liquido.toFixed(2));
        $('#id_conta_saldo').val(saldo.toFixed(2));
        $('#calculo-saldo-calculado').html(saldo.toFixed(2));
    }

    // atualizar saldo ao digitar o valor
    $('#id_valor_bruto').on('keyup', function() {
        calcular_tributos();
    });

    // tributos retidos omitidos inicialmente
    if (! $('#id_tributo_retido').prop('checked'))
        $('.field-tributos_retidos').css('display', 'none');

    function calcular_tributos() {
        var tributos_retidos = 0;
        $('.field-tributos_retidos')
            .find('input[type="checkbox"]:checked')
            .each(function(){
                text_tributo = $(this).parents('label').text();
                tributo = text_tributo.match(/\d+([\.,]\d+)?/)[0];
                tributo = parseFloat(tributo);
                tributos_retidos = tributos_retidos + tributo;
            });
        valor_bruto = parseFloat($('#id_valor_bruto').val());
        valor_tributos = ((tributos_retidos*valor_bruto) / 100);
        valor_liquido = valor_bruto - valor_tributos;
        $('#id_valor_tributo').val(valor_tributos.toFixed(2));
        atualizar_saldo(valor_liquido);
    }

    // recalculo dos tributos ao (des)habilitar algum tributo
    $('.field-tributos_retidos')
        .find('input[type="checkbox"]')
        .click(function(){ calcular_tributos(); });

    // mostrar/ocultar tributos retidos e recalcular
    $('#id_tributo_retido').click(function(e) {
        if ($(this).prop('checked')) {
            $('.field-tributos_retidos').css('display', 'block');
            $('.field-tributos_retidos')
                .find('input[type="checkbox"]')
                .each(function(){
                    $(this).prop('checked', true);
                });
        }
        else {
            $('.field-tributos_retidos')
                .find('input[type="checkbox"]')
                .each(function(){
                    $(this).prop('checked', false)
                });
            $('.field-tributos_retidos').css('display', 'none');
        }

        calcular_tributos();
    });

});

// obter saldo conta
// TODO aplicar somente na inserção
var conta_saldo = 0;
var valor_original = null;
var conta_saldo_lancamento_anterior = 0;
var valor_liquido = 0;

var consultar_conta_saldo = function() {
    var conta = $('#id_conta').val();

    // Checar "conta" caso esteja vazia.
    if (! conta) {
        return
    }

    data_lancto = $('#id_data').val();

    // Verificar data válida
    var dateRegex = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/;
    if (! dateRegex.test(data_lancto)) {
        return
    }

    var lancamento = $('#id_lancamento').val();
    var data = {
        lancamento: lancamento,
        data: $('#id_data').val()
    }

    var url = '/administrativo/api/conta/'+conta+'/saldo/'

    $.get(url, data, function(response) {
        if (response.status == 200) {
            if (lancamento) { // edição
                if (valor_original == null) {
                    conta_saldo = $('#id_conta_saldo').val();
                    var valor_original = $('#id_valor_bruto').val();
                    valor_liquido = $('#id_valor').val();
                }
                else {
                    conta_saldo = response.saldo;
                    $('#id_conta_saldo').val(conta_saldo);
                    $('#id_valor_bruto').val(valor_original);

                    $('#id_valor').val(valor_liquido);
                }
            }
            else { // inserção
                conta_saldo = response.saldo;
                $('#id_conta_saldo').val(conta_saldo);
                $('#calculo-saldo-data_movimento').html(conta_saldo);
                var valor_original = $('#id_valor_bruto').val();
            }

            conta_saldo = parseFloat(response.saldo)+parseFloat(valor_liquido);
            conta_saldo = conta_saldo.toFixed(2)
            $('#id_conta_saldo').val(conta_saldo);
            conta_saldo_lancamento_anterior = response.saldo;
            $('#calculo-saldo-conta').html(response.conta);
            $('#calculo-saldo-atual').html(response.saldo_atual);
            $('#calculo-data_movimento').html(response.saldo_data);
            $('#calculo-saldo-data_movimento').html(response.saldo);
            $('#calculo-saldo-calculado').html(conta_saldo);
            $('#calculo-valor_liquido').html(valor_liquido);
            $('#changeform-saldo').css('display', 'block');
        }
        else if (response.status) {
            alert('Erro na atualização do saldo!\n\n' + response.content);
        }
        else {
            console.log('Erro na atualização do saldo!\n\n' + response.content);
        }
    });
}

// $(document).on('change', '#id_conta', consultar_conta_saldo);
// $(document).on('keydown', '#id_data', consultar_conta_saldo);
setInterval(consultar_conta_saldo, 1500);


function gerar_livro_caixa () {
    // Obtem mês e ano referência do livro caixa
    mes_ano = prompt('Informe o mês e o ano no formato MM/YYYY. Ex.: 08/2022');
    if (! mes_ano) {
        return;
    }
    data_ref = mes_ano.split('/');
    if (data_ref.length!=2) {
        alert('Formato incorreto! Por favor, informe o mês e o ano no formato MM/YYYY. Ex.: 08/2022');
        gerar_livro_caixa();
    }
    // Preparar dados
    var data = {
        mes: data_ref[0],
        ano: data_ref[1]
    }
    var url = '/administrativo/api/livro_caixa/'

    //chama o ajax para gerar o livro caixa
    $.get(url, data)
        .done(function(data) {
            //redireciona para o arquivo do livro caixa gerado
            window.location=data.url_livro_caixa;
        })
        .fail(function() {
            alert('Formato incorreto! Por favor, informe o mês e o ano no formato MM/YYYY. Ex.: 08/2022');
            gerar_livro_caixa();
        })
        .always(function() {
            // alert( "finished" );
        });
}




function show_popup(triggeringLink, name_regexp, add_popup) {
    // const name = triggeringLink.id.replace(name_regexp, '');
    let href = triggeringLink.href;
    if (add_popup) {
        if (href.indexOf('?') === -1) {
            href += '?_popup=1';
        } else {
            href += '&_popup=1';
        }
    }

    var name = 'addMovimento';
    var viewportwidth = document.documentElement.clientWidth;
    var viewportheight = document.documentElement.clientHeight;
    var width = 354;
    window.resizeBy(width*(-1),0);
    window.moveTo(0,0);

    options = '\
    height="100%",\
    width='+width+',\
    left='+(viewportwidth-(width-10))+',\
    resizable=yes,\
    location=no,\
    menubar=no,\
    status=no,\
    titlebar=no,\
    scrollbars=no';
    const win = window.open(href, name, options);
    win.focus();
    return false;
}

// open "add movimento" on popup
/*
  $('.addlink').click(function(e) {
    e.preventDefault();
    var event = $.Event('django:lookup-related');
    $(this).trigger(event);
    if (!event.isDefaultPrevented()) {
      show_popup(this, /^lookup_/, true);
    }
  });
  */
