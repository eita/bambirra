.. Bambirra - Gestão do trabalho cooperativo documentation master file, created by
   sphinx-quickstart on Sun Jun  6 21:41:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bambirra - Gestão do trabalho cooperativo
=========================================

.. toctree::
   :maxdepth: 1
   :caption: Sumário:

   Resumo <README.md>
   Estrutura da aplicação <estrutura-aplicacao.rst>


Índices e tabelas
==================

* :ref:`genindex`
* :ref:`modindex`
