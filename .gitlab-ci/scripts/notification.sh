#!/bin/bash

# gitlab: Predefined variables reference
# https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
curl -XPOST -d '{"msgtype":"m.notice", "body":"Pipeline '"$CI_COMMIT_BRANCH"' '"$1"'! '"$CI_PIPELINE_URL"'"}' $CI_NOTIFY_MATRIX_URL
# curl -XPOST -d '{"msgtype":"m.notice", "body":"Pipeline '"$CI_COMMIT_BRANCH"' '"$1"'! '"$CI_PIPELINE_URL"' - '"$CI_COMMIT_AUTHOR"': '"$CI_COMMIT_MESSAGE"'"}' $CI_NOTIFY_MATRIX_URL
