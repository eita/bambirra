#!/bin/bash

mkdir -p docs
cp -r docs_src/* docs/
cp README.md docs/
cp docker-instructions.md docs/
cp bambirra/apps/fluxo_de_caixa/README.rst docs/estrutura-aplicacao.rst

# * Cadastro de cooperadas para uso da plataforma;
python manage.py graph_models fluxo_de_caixa -I Cooperada  > docs/modelo_fluxo_caixa-cadastro_acesso_cooperadas.dot

# * Registro de projetos e orçamentos;
python manage.py graph_models fluxo_de_caixa -I Cliente  > docs/modelo_fluxo_caixa-registro_projetos_orcamentos.dot

# * Gerenciamento das horas e prazo das atividades do orçamento aprovado;

# * Registro de horas trabalhadas por atividade de orçamento;

# * Registro de entradas e saídas financeiras em diferentes contas bancárias.
python manage.py graph_models fluxo_de_caixa -I ContaBancaria  > docs/modelo_fluxo_caixa-registro_projetos_orcamentos.dot

sphinx-apidoc -f -o docs . *migrations manage.py
sphinx-build -b html docs public
