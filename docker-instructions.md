# Bambirra / Docker

## Requirements

* Using Docker:
  * [Docker](https://docs.docker.com/engine/install/)
  * [Docker-Compose](https://docs.docker.com/compose/install/)
      * note about [compose upgrade](https://gitlab.com/eita/docker/docker_django/-/blob/master/docker-compose-upgrade.md)

* Using Podman (only dev):
  * [Podman](https://podman.io/getting-started/installation)
  * [Podman-Compose](https://github.com/containers/podman-compose#installation)


## Clone

```sh
git clone https://gitlab.com/eita/bambirra.git --recurse-submodules
cd bambirra
```


## Prepare files

1. Copy file *docker_django/app/.dockerignore* `cp docker_django/app/.dockerignore .`
1. Copy dir *docker_django/.envs/* `cp -r docker_django/.envs/ ./`


## Development

Uses the default Django development server.

1. Edit *./.envs/.env.dev* file to config.
1. Build image and run containers:

    ```sh
    ./docker-django.sh compose up -d
    ```

1. Using pdb to debug
    * check on compose if `stdin_open` and `tty` is `true`
    * Attach web container `./docker-django.sh attach`


## Production

Uses gunicorn + nginx + certbot.

1. Generate certificates

    ```sh
    docker run --rm \
    -p 443:443 -p 80:80 --name letsencrypt \
    -v "/etc/letsencrypt:/etc/letsencrypt" \
    -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
    certbot/certbot certonly -n \
    -m "YOUR_EMAIL" \
    -d example.com \
    --standalone --agree-tos
    ```

1. Configure automatic renewal using script `docker_django/certbot/certbot_updateall.sh`

1. Update the environment variables:
    * Edit .envs/.env.prod.*

1. Services

    ```sh
    ./docker-django.sh service start
    ```


## Commands to manage containers of Django project

Access `docker-django.sh` script documentation:

```sh
./docker-django.sh help
```


## Enable gitlab-ci

[read documentation](https://gitlab.com/eita/docker/docker_django/-/tree/master/.gitlab-ci/README.md)


## Credits

* [testdrivenio/django-on-docker](https://github.com/testdrivenio/django-on-docker)
* [finnian.io](https://finnian.io/blog/ssl-with-docker-swarm-lets-encrypt-and-nginx/)
* [offen/docker-volume-backup](https://github.com/offen/docker-volume-backup)
