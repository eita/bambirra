# Bambirra

Sistema financeiro

## Instalação

* clonar repositório `git clone https://gitlab.com/eita/bambirra.git`;
* criar virtualenv `virtualenv -p python3.6 venv && source venv/bin/activate`;
* instalar requerimentos `pip install -r requirements/local.txt`

## Importar dados

* faça download dos dados anteriores no formato (TSV)
* executar: `python manage.py import_csv [path-to-tsv-file] [path-to-tsv-projects-file]`

## Resetar base de dados

Sempre que necessário, basta executar `./reset_db.sh`
obs: caso o arquivo não tenha, atribua permissão: `chmod +x reset_db.sh`

